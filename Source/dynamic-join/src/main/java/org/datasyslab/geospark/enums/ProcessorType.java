package org.datasyslab.geospark.enums;

public enum ProcessorType{
    RISPS, RIHPS, SPIS, SPS
}
