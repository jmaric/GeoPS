package hr.fer.retrofit.dynamic_join.geops_util.statistics;

import java.util.ArrayList;
import java.util.List;

public class SimpleHost {
    private List<Integer> partitionIds;
    private long sum;

    public SimpleHost() {
        this.sum = 0;
        this.partitionIds = new ArrayList<>();
    }

    public void increaseSum(int id, long amount) {
        this.partitionIds.add(id);
        this.sum += amount;
    }

    public List<Integer> getPartitionIds() {
        return partitionIds;
    }

    public long getSum() {
        return this.sum;
    }

    @Override
    public String toString() {
        return "Partition{" +
                "elements=" + partitionIds +
                ", sum=" + sum +
                '}';
    }
}
