package hr.fer.retrofit.dynamic_join.geops_util.input_generator;

import hr.fer.retrofit.geofil.data.wrapping.AIdedPoint;
import org.locationtech.jts.index.SpatialIndex;

import java.util.Map;
import java.util.stream.Collectors;

public class RihpsPointPartitionsLocator implements PointPartitionsLocator{

    private SpatialIndex index;

    private int exactNumPartitions;

    public RihpsPointPartitionsLocator(SpatialIndex index, int exactNumPartitions){
        this.index = index;
        this.exactNumPartitions = exactNumPartitions;
    }

    @Override
    public Map<Integer, Integer> locatePartitions(AIdedPoint point) {
        Map<Integer, Integer> partitions = (Map<Integer, Integer>) index.query(point.getPoint().getEnvelopeInternal()).stream().map((Object se) -> ((Integer)se)%exactNumPartitions).collect(Collectors.groupingBy(x -> x, Collectors.summingInt(x -> 1)));
        return partitions;
    }
}
