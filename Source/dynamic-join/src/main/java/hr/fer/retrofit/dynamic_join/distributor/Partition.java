package hr.fer.retrofit.dynamic_join.distributor;

import com.google.common.collect.EvictingQueue;

import java.util.List;
import java.util.Queue;
import java.util.stream.Collectors;

public class Partition {

    private int id;

    private String location;

    private Queue<Long> lastCountsQueue;

    private long count;

    private final static int DEFAULT_COUNTS_QUEUE_SIZE = 30;

    public Partition() {
    }

    public Partition(int id, String location, long count) {
        this.location = location;
        this.count = count;
        this.lastCountsQueue = EvictingQueue.create(DEFAULT_COUNTS_QUEUE_SIZE);
        this.id = id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public long getCount() {
        return count;
    }

    public long getCountDifference(){
        List<Long> list = lastCountsQueue.stream().collect(Collectors.toList());
        if(list.size() == 0) return 0;
        if(list.size() == 1) return list.get(0);
        return list.get(list.size()-1) - list.get(0);
    }

    public long getLoad(){
        return getCountDifference();//count/countDifference
    }

    public void setCount(long count) {
        this.count = count;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Queue<Long> getLastCountsQueue() {
        return lastCountsQueue;
    }

    public void setLastCountsQueue(Queue<Long> lastCountsQueue) {
        this.lastCountsQueue = lastCountsQueue;
    }

    public static Partition of(int id, String location, long count){
        return new Partition(id, location, count);
    }

    @Override
    public String toString() {
        return "Partition{" +
                "location='" + location + '\'' +
                ", count=" + count +
                '}';
    }
}
