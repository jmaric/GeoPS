package hr.fer.retrofit.dynamic_join.geops_util.input_generator;

import hr.fer.retrofit.geofil.data.wrapping.AIdedPoint;

import java.util.Map;

public interface PointsDistributer {
    public AIdedPoint nextPoint();
    public Map<Integer, Integer> getPartitionPointsCounter();
}
