package hr.fer.retrofit.dynamic_join.geops_util.statistics;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public abstract class StatisticsFile {
    protected String filePath;
    protected List<String> text;
    protected String id;

    public StatisticsFile(String filePath){
        try{
            this.filePath = filePath;
            Configuration conf = new Configuration();
            Path path = new Path(filePath);
            FileSystem fs = path.getFileSystem(conf); // context of mapper or reducer
            FSDataInputStream fdsis = fs.open(path);
            BufferedReader br = new BufferedReader(new InputStreamReader(fdsis));
            String line = "";
            ArrayList<String> lines = new ArrayList<String>();
            while ((line = br.readLine()) != null) {
                lines.add(line);
            }
            br.close();
            this.id = lines.remove(0);
            this.text = lines;
        }catch(IOException ex){
            throw new RuntimeException(ex);
        }
    }

    public boolean isDataBalancing(){
        return id.contains(TDB_CONTAINS);
    }

    public boolean isStreamLocalityJoin(){
        return id.contains(STRL_CONTAINS);
    }

    public boolean isDynamic(){
        return id.contains(DYNAMIC_PROCESSOR_CONTAINS) && !id.contains(DYNAMIC_REPARTITION_FALSE);
    }

    public boolean isDynamicInactive(){
        return id.contains(DYNAMIC_PROCESSOR_CONTAINS) && id.contains(DYNAMIC_REPARTITION_FALSE);
    }

    public boolean isStatic(){
        return !id.contains(DYNAMIC_PROCESSOR_CONTAINS);
    }

    public int extractExperimentNumber(){
        return Integer.parseInt(id.split("nex:")[1].split(",")[0]);
    }

    public List<String> getText() {
        return text;
    }

    public void setText(List<String> text) {
        this.text = text;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    private static final String TDB_CONTAINS = "tdb:true";
    private static final String STRL_CONTAINS = "strl:true";
    private static final String DYNAMIC_PROCESSOR_CONTAINS = "Dynamic";
    private static final String DYNAMIC_REPARTITION_FALSE = "drb:false";
}
