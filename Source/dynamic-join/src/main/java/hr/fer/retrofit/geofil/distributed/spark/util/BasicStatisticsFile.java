package hr.fer.retrofit.geofil.distributed.spark.util;

import hr.fer.retrofit.dynamic_join.geops_util.statistics.StatisticsFile;

public class BasicStatisticsFile extends StatisticsFile {

    private static final String FINE_PRINT_THROUGHPUT_PREFIX = "Fine print throughput: ";
    private static final String FINE_PRINT_LATENCY_PREFIX = "Fine print latency: ";

    public BasicStatisticsFile(String filePath) {
        super(filePath);
    }

    public Double extractThroughput(){
        try{
            return Double.parseDouble(text.stream().filter(line -> line.startsWith(FINE_PRINT_THROUGHPUT_PREFIX)).findFirst()
                    .get().replaceFirst(FINE_PRINT_THROUGHPUT_PREFIX, "").split(" ")[1]);
        }catch(Exception ex){
            return null;
        }
    }

    public Double extractLatency(){
        try{
            return Double.parseDouble(text.stream().filter(line -> line.startsWith(FINE_PRINT_LATENCY_PREFIX)).findFirst()
                    .get().replaceFirst(FINE_PRINT_LATENCY_PREFIX, "").split(" ")[1]);
        }catch(Exception ex){
            return null;
        }
    }

}
