/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hr.fer.retrofit.geofil.distributed.spark.util;

import org.apache.spark.streaming.scheduler.BatchInfo;
import org.apache.spark.streaming.scheduler.StreamingListener;
import org.apache.spark.streaming.scheduler.StreamingListenerBatchCompleted;
import org.apache.spark.streaming.scheduler.StreamingListenerBatchStarted;
import org.apache.spark.streaming.scheduler.StreamingListenerBatchSubmitted;
import org.apache.spark.streaming.scheduler.StreamingListenerOutputOperationCompleted;
import org.apache.spark.streaming.scheduler.StreamingListenerOutputOperationStarted;
import org.apache.spark.streaming.scheduler.StreamingListenerReceiverError;
import org.apache.spark.streaming.scheduler.StreamingListenerReceiverStarted;
import org.apache.spark.streaming.scheduler.StreamingListenerReceiverStopped;
import org.apache.spark.streaming.scheduler.StreamingListenerStreamingStarted;
import org.apache.spark.util.CollectionAccumulator;
import org.apache.spark.util.LongAccumulator;
import scala.Tuple2;
import scala.Tuple3;

/**
 *
 * 
 */
public class StatsCollectorStreamingListener implements StreamingListener {

//    public static void main(String[] args){
//        List<Tuple2<Long, Double>> list = new ArrayList<Tuple2<Long, Double>>();
//        list.add(new Tuple2<Long, Double>(13L, 23.0));
//        list.add(new Tuple2<Long, Double>(15L, 23.2));
//        list.add(new Tuple2<Long, Double>(55L, 23.2));
//        list.add(new Tuple2<Long, Double>(65L, 23.4));
//        list.stream().collect(Collectors.groupingBy(t -> (t._1/50)*50, Collectors.averagingDouble(t -> t._2)))
//                .entrySet().stream().map(e -> new Tuple2<>(e.getKey(), e.getValue()))
//                .forEach(t -> System.out.println(t._1 + "," + String.format("%.2f", t._2)));
//
//    }

    private final LongAccumulator numRecords;
    private final LongAccumulator processingDelay;
    private final MinLongAccumulator minTime;
    private final MaxLongAccumulator maxTime;
    private final CollectionAccumulator<Tuple3<Long, Long, Long>> throughputLatencyInSec;

    public StatsCollectorStreamingListener(LongAccumulator numRecords, LongAccumulator processingDelay, MinLongAccumulator minTime, MaxLongAccumulator maxTime,
                                           CollectionAccumulator<Tuple3<Long, Long, Long>> throughputLatencyInSec) {
        this.numRecords = numRecords;
        this.processingDelay = processingDelay;
        this.maxTime = maxTime;
        this.minTime = minTime;
        this.throughputLatencyInSec = throughputLatencyInSec;
    }

    @Override
    public void onStreamingStarted(StreamingListenerStreamingStarted slss) {
    }

    @Override
    public void onReceiverStarted(StreamingListenerReceiverStarted slrs) {
    }

    @Override
    public void onReceiverError(StreamingListenerReceiverError slre) {
    }

    @Override
    public void onReceiverStopped(StreamingListenerReceiverStopped slrs) {
    }

    @Override
    public void onBatchSubmitted(StreamingListenerBatchSubmitted slbs) {
        slbs.batchInfo();
    }

    @Override
    public void onBatchStarted(StreamingListenerBatchStarted slbs) {
        BatchInfo batchInfo = slbs.batchInfo();
        
        if (batchInfo.numRecords() > 0) {
            minTime.add((long) batchInfo.processingStartTime().get());
        }
    }

    @Override
    public void onBatchCompleted(StreamingListenerBatchCompleted slbc) {
        BatchInfo batchInfo = slbc.batchInfo();

        long batchNumRecords = batchInfo.numRecords();

        System.out.println("Batch completed");

        System.out.println("batchNumRecords: " + batchNumRecords);

        if (batchNumRecords > 0) {
            numRecords.add(batchNumRecords);
            processingDelay.add((long) batchInfo.processingDelay().get());
            maxTime.add((long) batchInfo.processingEndTime().get());

            long passedTimeInSecs = ((long)batchInfo.processingEndTime().get()-minTime.value())/1000;
//            throughput.add(new Tuple2<>(((long)batchInfo.processingEndTime().get()-minTime.value())/1000, batchNumRecords*1000.0/(((long)batchInfo.processingEndTime().get()-(long)batchInfo.processingStartTime().get()))));
            throughputLatencyInSec.add(new Tuple3<>(((long)batchInfo.processingEndTime().get()-minTime.value())/1000, (long)batchNumRecords, (long)batchInfo.processingDelay().get()));

//            if(passedTimeInSecs >= throughputPeriodTime.value()){
//                long periodTime = throughputPeriodTime.value();
//                throughput.add(new Tuple2<>(periodTime, throughputPeriodRecords.value()*1.0/THROUGHPUT_PERIOD));
//                throughputPeriodTime.add(periodTime + THROUGHPUT_PERIOD);
//                throughputPeriodRecords.reset();
//                throughputPeriodRecords.add(batchNumRecords);
//            }else{
//                throughputPeriodRecords.add(batchNumRecords);
//            }
        }
    }

    @Override
    public void onOutputOperationStarted(StreamingListenerOutputOperationStarted sloos) {
    }

    @Override
    public void onOutputOperationCompleted(StreamingListenerOutputOperationCompleted slooc) {
    }
}
