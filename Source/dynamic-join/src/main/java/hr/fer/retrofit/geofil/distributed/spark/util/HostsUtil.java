package hr.fer.retrofit.geofil.distributed.spark.util;

import org.apache.spark.SparkContext;
import org.apache.spark.storage.BlockManagerId;
import scala.collection.JavaConverters;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class HostsUtil {
    public static final List<String> NODE_IPS = Arrays.asList(
            "worker01.streamslab.fer.hr",
            "worker02.streamslab.fer.hr",
            "worker03.streamslab.fer.hr",
            "worker04.streamslab.fer.hr",
            "worker05.streamslab.fer.hr",
            "worker06.streamslab.fer.hr",
            "worker07.streamslab.fer.hr",
            "worker08.streamslab.fer.hr",
            "worker09.streamslab.fer.hr",
            "worker10.streamslab.fer.hr",
            "worker11.streamslab.fer.hr",
//            "worker12.streamslab.fer.hr",
            "worker13.streamslab.fer.hr",
            "worker14.streamslab.fer.hr",
            "worker15.streamslab.fer.hr",
            "worker16.streamslab.fer.hr"
    );

    public static List<String> getExecutorLocationsFromContext(SparkContext sc){
        List<String> locations = ((Set<BlockManagerId>)(JavaConverters.mapAsJavaMapConverter(sc.env().blockManager().master().getMemoryStatus()).asJava()).
                    keySet()).stream().map((BlockManagerId bm) -> ("executor_" + bm.host() + "_" + bm.executorId())).collect(Collectors.toList());
        locations = locations.stream().filter(ex -> !ex.contains("driver")).collect(Collectors.toList());
        return locations;
    }

    public static List<String> getExecutorHostsFromContext(SparkContext sc){
        List<String> locations = ((Set<BlockManagerId>)(JavaConverters.mapAsJavaMapConverter(sc.env().blockManager().master().getMemoryStatus()).asJava()).
                keySet()).stream().filter(bm -> !bm.executorId().contains("driver")).map((BlockManagerId bm) -> (bm.host())).sorted().collect(Collectors.toList());
        return locations;
    }
}
