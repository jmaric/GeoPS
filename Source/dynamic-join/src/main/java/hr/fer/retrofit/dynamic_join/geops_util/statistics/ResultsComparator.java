package hr.fer.retrofit.dynamic_join.geops_util.statistics;

import hr.fer.retrofit.geofil.distributed.kafka.SimpleKafkaTopicConsumer;
import scala.Tuple2;

import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

public class ResultsComparator {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ExecutorService pool = Executors.newFixedThreadPool(2);
        SimpleKafkaTopicConsumer consumer1 = new SimpleKafkaTopicConsumer("geofil-matchings");
        Future<List<Tuple2<Integer, Set<Integer>>>> res1 = pool.submit(consumer1);
        List<Tuple2<Integer, List<Integer>>> res1Got = res1.get().stream().map(t -> new Tuple2<Integer, List<Integer>>(t._1, t._2.stream().sorted(Comparator.comparingInt(t2 -> (int) t2)).collect(Collectors.toList()))).sorted((t1, t2) -> Integer.compare((int)t1._1, (int)(t2._1))).collect(Collectors.toList());
        System.out.println("First:");
        System.out.println(res1Got.stream().limit(1000).collect(Collectors.toList()));
        SimpleKafkaTopicConsumer consumer2 = new SimpleKafkaTopicConsumer("geofil-matchings");
        Future<List<Tuple2<Integer, Set<Integer>>>> res2 = pool.submit(consumer2);
        List<Tuple2<Integer, List<Integer>>> res2Got = res2.get().stream().map(t -> new Tuple2<Integer, List<Integer>>(t._1, t._2.stream().sorted(Comparator.comparingInt(t2 -> (int) t2)).collect(Collectors.toList()))).sorted((t1, t2) -> Integer.compare((int)t1._1, (int)(t2._1))).collect(Collectors.toList());
        System.out.println("Second:");
        System.out.println(res2Got.stream().limit(1000).collect(Collectors.toList()));
        System.out.println("Results are equal: " + res1Got.equals(res2Got));
    }
//
//    public static void main(String[] args) throws ExecutionException, InterruptedException {
//        List<Tuple2<Integer, Set<Integer>>> res1Got = Arrays.asList(
//                new Tuple2<Integer, Set<Integer>>(5, new HashSet<>(Arrays.asList(5,3,2,4,1))),
//                new Tuple2<Integer, Set<Integer>>(2, new HashSet<>(Arrays.asList(2,4,1,7)))).stream().sorted((t1,t2) -> Integer.compare((int)t1._1, (int)(t2._1))).collect(Collectors.toList());
//        List<Tuple2<Integer, Set<Integer>>> res2Got = Arrays.asList(
//                new Tuple2<Integer, Set<Integer>>(2, new HashSet<>(Arrays.asList(2,1,7,4))),
//                new Tuple2<Integer, Set<Integer>>(5, new HashSet<>(Arrays.asList(2,5,3,4,1)))).stream().sorted((t1,t2) -> Integer.compare((int)t1._1, (int)(t2._1))).collect(Collectors.toList());
//        System.out.println("First:");
//        System.out.println(res1Got);
//        System.out.println("Second:");
//        System.out.println(res2Got);
//        System.out.println("Results are equal: " + res1Got.equals(res2Got));
//    }
}
