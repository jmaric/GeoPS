package hr.fer.retrofit.dynamic_join.geops_util.statistics;

import hr.fer.retrofit.geofil.distributed.spark.AbstractProcessor;
import hr.fer.retrofit.geofil.distributed.spark.util.BasicStatisticsFile;
import hr.fer.retrofit.geofil.distributed.spark.util.BasicStatisticsFileSet;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.stream.Collectors;

public class StatisticsComparator {
    public static void main(String[] args){
        if (args.length != 6 && args.length != 7) {
            System.out.println("StatisticsComparator experiment_num_1 experiment_num_2 experiment_num_3 experiment_num_4 experiment_num_5 [comparation_name]");
            System.exit(1);
        }

        BasicStatisticsFileSet basicStatisticsFileSet = new BasicStatisticsFileSet(new HashSet<>(
            Arrays.asList(
                    new BasicStatisticsFile(getExperimentPath(args[0])),
                    new BasicStatisticsFile(getExperimentPath(args[1])),
                    new BasicStatisticsFile(getExperimentPath(args[2])),
                    new BasicStatisticsFile(getExperimentPath(args[3])),
                    new BasicStatisticsFile(getExperimentPath(args[4])),
                    new BasicStatisticsFile(getExperimentPath(args[5]))
            )));
        TimeValuePairsStatisticsFileSet throughputsStatisticsFileSet = new TimeValuePairsStatisticsFileSet(new HashSet<>(
                Arrays.asList(
                        new TimeValuePairsStatisticsFile(getThroughputPath(args[0])),
                        new TimeValuePairsStatisticsFile(getThroughputPath(args[1])),
                        new TimeValuePairsStatisticsFile(getThroughputPath(args[2])),
                        new TimeValuePairsStatisticsFile(getThroughputPath(args[3])),
                        new TimeValuePairsStatisticsFile(getThroughputPath(args[4])),
                        new TimeValuePairsStatisticsFile(getThroughputPath(args[5]))
                )));
        TimeValuePairsStatisticsFileSet latenciesStatisticsFileSet = new TimeValuePairsStatisticsFileSet(new HashSet<>(
                Arrays.asList(
                        new TimeValuePairsStatisticsFile(getLatencyPath(args[0])),
                        new TimeValuePairsStatisticsFile(getLatencyPath(args[1])),
                        new TimeValuePairsStatisticsFile(getLatencyPath(args[2])),
                        new TimeValuePairsStatisticsFile(getLatencyPath(args[3])),
                        new TimeValuePairsStatisticsFile(getLatencyPath(args[4])),
                        new TimeValuePairsStatisticsFile(getLatencyPath(args[5]))
                )));

        if(args.length > 6) {
            basicStatisticsFileSet.setComparationName(args[6]);
            throughputsStatisticsFileSet.setComparationName(args[6]);
            latenciesStatisticsFileSet.setComparationName(args[6]);
        }
        basicStatisticsFileSet.writeComparationToHdfs();
        throughputsStatisticsFileSet.writeCollectiveTimeValuePairsToHdfs(AbstractProcessor.HDFS_THROUGHPUTS_RELATIVE_PATH);
        latenciesStatisticsFileSet.writeCollectiveTimeValuePairsToHdfs(AbstractProcessor.HDFS_LATENCIES_RELATIVE_PATH);

        System.out.println(Collections.max(basicStatisticsFileSet.getAllFiles().stream().map(file -> ((BasicStatisticsFile)file).extractThroughput()).collect(Collectors.toList())));
    }

    public static String getExperimentPath(String numberOfExperiment){
        return "hdfs://"
                + "primary:8020" + AbstractProcessor.HDFS_EXPERIMENT_RELATIVE_PATH +
                numberOfExperiment + ".txt";
    }

    public static String getThroughputPath(String numberOfExperiment){
        return "hdfs://"
                + "primary:8020" + AbstractProcessor.HDFS_THROUGHPUT_RELATIVE_PATH +
                numberOfExperiment + ".txt";
    }

    public static String getMldcPath(String numberOfExperiment){
        return "hdfs://"
                + "primary:8020" + AbstractProcessor.HDFS_MLDC_RELATIVE_PATH +
                numberOfExperiment + ".txt";
    }


    public static String getLatencyPath(String numberOfExperiment){
        return "hdfs://"
                + "primary:8020" + AbstractProcessor.HDFS_LATENCY_RELATIVE_PATH +
                numberOfExperiment + ".txt";
    }


}
