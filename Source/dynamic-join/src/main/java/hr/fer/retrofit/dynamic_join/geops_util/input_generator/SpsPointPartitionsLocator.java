package hr.fer.retrofit.dynamic_join.geops_util.input_generator;

import hr.fer.retrofit.geofil.data.wrapping.AIdedPoint;
import org.datasyslab.geospark.spatialPartitioning.SpatialPartitioner;
import scala.Tuple2;

import java.util.Map;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class SpsPointPartitionsLocator implements PointPartitionsLocator{

    private SpatialPartitioner partitioner;

    public SpsPointPartitionsLocator(SpatialPartitioner partitioner){
        this.partitioner = partitioner;
    }

    @Override
    public Map<Integer, Integer> locatePartitions(AIdedPoint point) {
        Map<Integer, Integer> partitions = null;
        try{
            partitions = StreamSupport.stream(
                    Spliterators.spliteratorUnknownSize(partitioner.placeObject(point.getPoint()), Spliterator.ORDERED), false)
                    .map((Object se) -> ((Integer)((Tuple2<?, ?>) se)._1)).collect(Collectors.groupingBy(x -> x, Collectors.summingInt(x -> 1)));
        }catch(Exception ex){System.out.println(ex);}
        return partitions;
    }
}
