package hr.fer.retrofit.dynamic_join.geops_util.statistics;

import java.util.HashMap;
import java.util.Map;

public class TimeValuePairsStatisticsFile extends StatisticsFile{

    public TimeValuePairsStatisticsFile(String filePath) {
        super(filePath);
    }

    public Map<Integer, Double> readTimeValuePairs(){
        Map<Integer, Double> throughputsByTime = new HashMap<>();
        text.stream().forEach(line -> {
            String[] splitted = line.split(",");
            throughputsByTime.put(Integer.parseInt(splitted[0].trim()), Double.parseDouble(splitted[1].trim()));
        });
        return throughputsByTime;
    }

}
