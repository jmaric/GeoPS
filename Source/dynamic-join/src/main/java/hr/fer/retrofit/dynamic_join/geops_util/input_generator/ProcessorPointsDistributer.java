package hr.fer.retrofit.dynamic_join.geops_util.input_generator;

import hr.fer.retrofit.geofil.data.wrapping.AIdedPoint;
import org.datasyslab.geospark.enums.ProcessorType;
import org.datasyslab.geospark.spatialPartitioning.SpatialPartitioner;
import org.locationtech.jts.index.SpatialIndex;
import scala.Tuple2;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class ProcessorPointsDistributer implements PointsDistributer{

    private ProcessorType processorType;
    private double skewThresholdPercentage;
    private int skewPointJobs;
    private int totalPointJobs;
    private Map<Integer, Integer> partitionPointsCounter;
    private int workers;
    private List<Integer> skewPartitions;
    private List<Tuple2<AIdedPoint, PartitionsInfo>> pointsToPartitions;
    private List<Tuple2<AIdedPoint, PartitionsInfo>> skewPointsToPartitions;
    private Map<Integer, Double> harderBoundaryPartitions;

    public ProcessorPointsDistributer(List<AIdedPoint> points, double skewThresholdPercentage, int skewWorkers, SpatialIndex index,
                                      int workers, int approximateNumPartitions, int exactNumPartitions, SpatialPartitioner partitioner,
                                      ProcessorType processorType) {
        this.processorType = processorType;
        this.skewThresholdPercentage = skewThresholdPercentage;
        this.workers = workers;
        this.skewPointJobs = 0;
        this.totalPointJobs = 0;
        this.partitionPointsCounter = preparePointsPartitionsCounterMap(exactNumPartitions);
        this.skewPartitions = fillOutSkewPartitions(processorType, skewWorkers, approximateNumPartitions);
        this.pointsToPartitions = calculatePartitions(points, index, exactNumPartitions, partitioner);
        System.out.println("TP: " + pointsToPartitions.size());
        this.skewPointsToPartitions = pointsToPartitions.stream().filter(tup -> tup._2.getPointSkewPercentage() > skewThresholdPercentage).collect(Collectors.toList());
        this.harderBoundaryPartitions = fillOutHarderBoundaryPartitions(processorType, skewWorkers, approximateNumPartitions);
        System.out.println("SP: " + skewPointsToPartitions.size());
    }

    @Override
    public AIdedPoint nextPoint(){
        long startTime = System.currentTimeMillis();
        List<Integer> forbiddenPartitions = getPartitionsBeingTooLarge(1.0/(workers));
        Tuple2<AIdedPoint, PartitionsInfo> pointToPartitionsInfo;
        boolean skewedEnough = (totalPointJobs == 0) || ((skewPointJobs*1.0/totalPointJobs) > skewThresholdPercentage);
        do{
            pointToPartitionsInfo = generateRandomPoint(!skewedEnough);
        }while(hasForbiddenPartitions(pointToPartitionsInfo._2.getPartitionsJobCountMap(), forbiddenPartitions));
        addPointToStats(pointToPartitionsInfo._2.getPartitionsJobCountMap());
//        System.out.println("nextPoint duration: " + (System.currentTimeMillis() - startTime));
        return pointToPartitionsInfo._1;
    }

    @Override
    public Map<Integer, Integer> getPartitionPointsCounter(){
        System.out.println("Total: " + totalPointJobs + ", skew: " + skewPointJobs);
        return partitionPointsCounter;
    }

    public Tuple2<AIdedPoint, PartitionsInfo> generateRandomPoint(boolean skewed){
        List<Tuple2<AIdedPoint, PartitionsInfo>> pointsListToPartitions = (skewed ? skewPointsToPartitions : pointsToPartitions);
        int pointIndex = ThreadLocalRandom.current().nextInt(pointsListToPartitions.size());
        return pointsListToPartitions.get(pointIndex);
    }

    public List<Integer> getPartitionsBeingTooLarge(double defaultRatio){
        int maxPointJobs = (int)(totalPointJobs*defaultRatio);

        return partitionPointsCounter.entrySet().stream().filter(entry -> {
            int partition = entry.getKey();
            int pointJobs = entry.getValue();
            if(harderBoundaryPartitions.keySet().contains(partition)){
                return pointJobs > harderBoundaryPartitions.get(partition)*maxPointJobs;
            }else{
                return pointJobs > maxPointJobs;
            }
                })
                .map(entry -> entry.getKey()).collect(Collectors.toList());
    }

    public boolean hasForbiddenPartitions(Map<Integer, Integer> partitionsMapForPoint, List<Integer> forbiddenPartitions){
        return partitionsMapForPoint.keySet().stream()
                .filter(partition -> forbiddenPartitions.contains(partition)).count()!=0;
    }

    public void addPointToStats(Map<Integer, Integer> partitionsMap){
        partitionsMap.entrySet().stream().forEach(entry -> {
            int partition = entry.getKey();
            int jobsCount = entry.getValue();

            if(skewPartitions.contains(partition)){
                skewPointJobs+=jobsCount;
            }
            totalPointJobs+=jobsCount;

            partitionPointsCounter.put(partition, partitionPointsCounter.getOrDefault(partition, 0) + jobsCount);
        });
    }

    public List<Tuple2<AIdedPoint, PartitionsInfo>> calculatePartitions(List<AIdedPoint> points, SpatialIndex index, int exactNumPartitions, SpatialPartitioner partitioner){
        PointPartitionsLocator pointPartitionsLocator = getCorrespondingPointPartitionsLocator(index, exactNumPartitions, partitioner);
        List<Tuple2<AIdedPoint, PartitionsInfo>> codedPointsPartitions = points.stream()
                .map(point -> {
                    Map<Integer, Integer> partitionsJobCountMap = pointPartitionsLocator.locatePartitions(point);
                    return new Tuple2<>(point, new PartitionsInfo(partitionsJobCountMap, skewPartitions));
                })
//                .filter(filterPointPartitionsInfoPredicate())
                .collect(Collectors.toList());

//        System.out.println(codedPointsPartitions);
//        System.out.println(codedPointsPartitions.stream().sorted(Comparator.comparingDouble(t -> t._2.getPointSkewPercentage())).collect(Collectors.toList()));
        return codedPointsPartitions;
    }

    private PointPartitionsLocator getCorrespondingPointPartitionsLocator(SpatialIndex index, int exactNumPartitions, SpatialPartitioner partitioner){
        switch(processorType){
            case RISPS:
                return new RispsPointPartitionsLocator(index);
            case RIHPS:
                return new RihpsPointPartitionsLocator(index, exactNumPartitions);
            case SPIS:
                return new SpisPointPartitionsLocator(partitioner);
            case SPS:
                return new SpsPointPartitionsLocator(partitioner);
        }
        return null;
    }

    public Map<Integer, Integer> preparePointsWorkersCounterMap(int workers){
        return resetMapWithIndexes(workers);
    }

    public Map<Integer, Integer> preparePointsPartitionsCounterMap(int numPartitions){
        return resetMapWithIndexes(numPartitions);
    }

    public static Map<Integer, Integer> resetMapWithIndexes(int num){
        Map<Integer, Integer> map = new HashMap<>();
        for(int i = 0; i < num; i++){
            map.put(i, 0);
        }
        return map;
    }

    public Predicate<Tuple2<AIdedPoint, PartitionsInfo>> filterPointPartitionsInfoPredicate(){
            return (Tuple2<AIdedPoint, PartitionsInfo> t) -> {
                double completeCount = t._2.getPartitionsJobCountMap().values().stream().count();
                return t._2.getPartitionsJobCountMap().entrySet().stream().filter(e -> {
                    int partition = e.getKey();
                    int count = e.getValue();
                    return ((partition == 139 && count/completeCount > 1.0/(3*workers)) ||
                            (partition == 197 && count/completeCount > 1.0/(8*workers)) || (partition == 0 && count/completeCount > 1.0/(4*workers)) || (partition == 198 && count/completeCount > 1.0/(2*workers))) ||
                            (partition == 2 && count/completeCount > 1.0/(5*workers)) ||
                            (partition == 124 && count/completeCount > 1.0/(6*workers));
                }).count() == 0;
            };
    }

    public Map<Integer, Double> fillOutHarderBoundaryPartitions(ProcessorType processorType, int skewWorkers, int approximateNumPartitions){
        Map<Integer, Double> map = new HashMap<>();
        if((processorType == ProcessorType.SPS) && approximateNumPartitions == 100){
            if(skewWorkers > 0){
//                map.put(0, 1.0/5);
                map.put(197, 1.0/10);
                map.put(24, 1.0/10);
                map.put(38, 1.0/10);
                map.put(0, 1.0/10);
                map.put(2, 1.0/10);
                map.put(99, 1.0/3);
            }
            if(skewWorkers > 1){

            }
            if(skewWorkers > 2){
                map.put(19, 1.0/4);
//                map.put(2, 1.0/9);
            }
        }else if((processorType == ProcessorType.SPIS) && approximateNumPartitions == 100){
            if(skewWorkers > 0){
                map.put(0, 1.0/5);
            }
            if(skewWorkers > 1){

            }
            if(skewWorkers > 2){
                map.put(19, 1.0/4);
                map.put(2, 1.0/15);
                map.put(38, 1.0/4);
            }
        }else if(processorType == ProcessorType.RISPS && approximateNumPartitions == 200){
            map.put(139, 1.0/5);
            map.put(24, 1.0/2);
            if(skewWorkers > 0){
                map.put(197, 1.0/16);
                map.put(0, 1.0/4);
                map.put(198, 1.0/2);
            }
            if(skewWorkers > 1){
                map.put(2, 1.0/5);
            }
            if(skewWorkers > 2){
                map.put(124, 1.0/18);
            }
        }else if(processorType == ProcessorType.RIHPS && approximateNumPartitions == 100){
//            map.put(11, 1.0/2);
//            map.put(94, 1.0/2);
            if(skewWorkers > 0){
//                map.put(16, 1.0/2);
//                map.put(66, 1.0/2);
            }
            if(skewWorkers > 1){
                map.put(17, 1.0/7);
            }
            if(skewWorkers > 2){
            }
        }
        return map;
    }

    public static List<Integer> fillOutSkewPartitions(ProcessorType processorType, int skewWorkers, int approximatePartitionsNum) {
        List<Integer> skewPartitions = new ArrayList<>();

        if (skewWorkers > 3) throw new IllegalArgumentException("Illegal number of workers for skewed data");
        if ((processorType == ProcessorType.SPS) && approximatePartitionsNum == 100) {
            if (skewWorkers > 0) {
                skewPartitions.add(0);
                skewPartitions.add(17);
                skewPartitions.add(34);
                skewPartitions.add(52);
                skewPartitions.add(70);
                skewPartitions.add(89);//worker01 partitions
            }
            if (skewWorkers > 1) {
                skewPartitions.add(1);
                skewPartitions.add(18);
                skewPartitions.add(35);
                skewPartitions.add(53);
                skewPartitions.add(71);
                skewPartitions.add(90);                                                    //worker02 partitions
            }
            if (skewWorkers > 2) {
                skewPartitions.add(2);
                skewPartitions.add(19);
                skewPartitions.add(36);
                skewPartitions.add(54);
                skewPartitions.add(72);
                skewPartitions.add(91);                                           //worker03 partitions
            }
        }else if(processorType == ProcessorType.SPIS && approximatePartitionsNum == 100){
            if (skewWorkers > 0) {
                skewPartitions.add(0);
                skewPartitions.add(18);
                skewPartitions.add(36);
                skewPartitions.add(54);
                skewPartitions.add(72);
                skewPartitions.add(90);//worker01 partitions
            }
            if (skewWorkers > 1) {
                skewPartitions.add(1);
                skewPartitions.add(19);
                skewPartitions.add(37);
                skewPartitions.add(55);
                skewPartitions.add(73);
                skewPartitions.add(91);
                skewPartitions.add(92);//worker02 partitions
            }
            if (skewWorkers > 2) {
                skewPartitions.add(2);
                skewPartitions.add(20);
                skewPartitions.add(38);
                skewPartitions.add(56);
                skewPartitions.add(74);
                skewPartitions.add(75);
                skewPartitions.add(93);                                           //worker03 partitions
            }
        }else if(processorType == ProcessorType.RISPS && approximatePartitionsNum == 100){
            if(skewWorkers > 0){
                skewPartitions.add(10);            skewPartitions.add(28);            skewPartitions.add(46);
                skewPartitions.add(82);                                                                                      //worker10 partitions
            }
            if(skewWorkers > 1){
                skewPartitions.add(2);            skewPartitions.add(19);            skewPartitions.add(72);
                skewPartitions.add(90);                                                                                       //worker02 partitions
            }
            if(skewWorkers > 2){
                skewPartitions.add(17);            skewPartitions.add(18);            skewPartitions.add(71);
                skewPartitions.add(89);                                                                                       //worker01 partitions
            }
        }else if(processorType == ProcessorType.RISPS && approximatePartitionsNum == 200){
            if(skewWorkers > 0){
                skewPartitions.add(0);            skewPartitions.add(1);            skewPartitions.add(118);
                skewPartitions.add(119);            skewPartitions.add(158);            skewPartitions.add(159);
                skewPartitions.add(197);            skewPartitions.add(198);            skewPartitions.add(39);
                skewPartitions.add(40);            skewPartitions.add(79);            skewPartitions.add(80);//worker01 partitions
            }
            if(skewWorkers > 1){
                skewPartitions.add(120);            skewPartitions.add(121);            skewPartitions.add(122);
                skewPartitions.add(160);            skewPartitions.add(161);            skewPartitions.add(199);
                skewPartitions.add(2);            skewPartitions.add(200);            skewPartitions.add(201);
                skewPartitions.add(3);            skewPartitions.add(41);            skewPartitions.add(42);
                skewPartitions.add(43);            skewPartitions.add(81);            skewPartitions.add(82);//worker02 partitions

            }
            if(skewWorkers > 2){
                skewPartitions.add(123);
                skewPartitions.add(124);            skewPartitions.add(162);
                skewPartitions.add(163);            skewPartitions.add(164);
                skewPartitions.add(202);
                skewPartitions.add(203);            skewPartitions.add(44);            skewPartitions.add(44);
                skewPartitions.add(45);            skewPartitions.add(5);            skewPartitions.add(6);
                skewPartitions.add(83);            skewPartitions.add(84);            skewPartitions.add(85);//worker03 partitions
            }
        }else if(processorType == ProcessorType.RIHPS && approximatePartitionsNum == 100){
            if(skewWorkers > 0){
                skewPartitions.add(0);            skewPartitions.add(16);            skewPartitions.add(33);
                skewPartitions.add(50);            skewPartitions.add(66);            skewPartitions.add(83);//worker01 partitions
            }
            if(skewWorkers > 1){
                skewPartitions.add(1);            skewPartitions.add(17);            skewPartitions.add(34);
                skewPartitions.add(51);            skewPartitions.add(67);            skewPartitions.add(84);//worker02 partitions
            }
            if(skewWorkers > 2){
                skewPartitions.add(2);            skewPartitions.add(18);            skewPartitions.add(35);
                skewPartitions.add(52);            skewPartitions.add(68);            skewPartitions.add(85);//worker03 partitions
            }
        }else if(processorType == ProcessorType.RIHPS && approximatePartitionsNum == 200){
            System.out.println("HASH:");
            if(skewWorkers > 0){
                skewPartitions.add(0);            skewPartitions.add(1);            skewPartitions.add(33);
                skewPartitions.add(34);            skewPartitions.add(67);            skewPartitions.add(66);
                skewPartitions.add(100);            skewPartitions.add(101);            skewPartitions.add(133);
                skewPartitions.add(134);            skewPartitions.add(166);            skewPartitions.add(167);//worker01 partitions
            }
            if(skewWorkers > 1){
                skewPartitions.add(2);            skewPartitions.add(3);            skewPartitions.add(35);
                skewPartitions.add(36);            skewPartitions.add(68);            skewPartitions.add(69);
                skewPartitions.add(102);            skewPartitions.add(103);            skewPartitions.add(135);
                skewPartitions.add(136);            skewPartitions.add(168);            skewPartitions.add(169);//worker02 partitions
            }
            if(skewWorkers > 2){
                skewPartitions.add(4);            skewPartitions.add(5);            skewPartitions.add(37);
                skewPartitions.add(38);            skewPartitions.add(70);            skewPartitions.add(71);
                skewPartitions.add(104);            skewPartitions.add(105);            skewPartitions.add(137);
                skewPartitions.add(138);            skewPartitions.add(170);            skewPartitions.add(171);//worker03 partitions
            }
    }
        return skewPartitions;
    }

}
