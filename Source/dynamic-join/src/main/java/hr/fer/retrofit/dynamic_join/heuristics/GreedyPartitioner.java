package hr.fer.retrofit.dynamic_join.heuristics;

import hr.fer.retrofit.dynamic_join.geops_util.statistics.SimpleHost;
import hr.fer.retrofit.dynamic_join.SimplePartitionComparator;
import org.apache.commons.math3.stat.descriptive.moment.Skewness;

import java.util.*;

import static java.util.Collections.reverseOrder;

public class GreedyPartitioner {

    public GreedyPartitioner(){

    }

    public static void main(String[] args){
        System.out.println(new Skewness().evaluate(new double[]{0,1,2,3,4,5,6,17,0,7,3,2,2}, 0, 13));
        System.out.println(improvedPartition(Arrays.asList(5,7,21,4,2,17,6,2,5,25,52), 3));
    }

    public static Collection<SimpleHost> improvedPartition(Collection<Integer> numbers, int partitionCount) {
        List<Integer> numbersCopy = new ArrayList<>(numbers);
        Collections.sort(numbersCopy, reverseOrder());
        Queue<Integer> numberQueue = new ArrayDeque<>(numbersCopy);

        PriorityQueue<SimpleHost> partitionPriorityQueue =
                new PriorityQueue<>(partitionCount, new SimplePartitionComparator());
        for (int i = 0; i < partitionCount; i++) {
            partitionPriorityQueue.add(new SimpleHost());
        }

        while (!numberQueue.isEmpty()) {
            Integer number = numberQueue.poll();
            SimpleHost lowestSumPartition = partitionPriorityQueue.poll();
            lowestSumPartition.increaseSum(number, number);
            partitionPriorityQueue.add(lowestSumPartition);
        }
        return partitionPriorityQueue;
    }

    public static Collection<SimpleHost> partitionNodes(Collection<Integer> numbers, int partitionCount) {
        List<Integer> numbersCopy = new ArrayList<>(numbers);
        Collections.sort(numbersCopy, reverseOrder());
        Queue<Integer> numberQueue = new ArrayDeque<>(numbersCopy);

        PriorityQueue<SimpleHost> partitionPriorityQueue =
                new PriorityQueue<>(partitionCount, new SimplePartitionComparator());
        for (int i = 0; i < partitionCount; i++) {
            partitionPriorityQueue.add(new SimpleHost());
        }

        while (!numberQueue.isEmpty()) {
            Integer number = numberQueue.poll();
            SimpleHost lowestSumPartition = partitionPriorityQueue.poll();
            lowestSumPartition.increaseSum(number, number);
            partitionPriorityQueue.add(lowestSumPartition);
        }
        return partitionPriorityQueue;
    }
}
