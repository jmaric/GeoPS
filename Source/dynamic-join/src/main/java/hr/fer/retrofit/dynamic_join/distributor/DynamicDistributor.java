package hr.fer.retrofit.dynamic_join.distributor;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import hr.fer.retrofit.geofil.distributed.spark.AbstractProcessor;
import hr.fer.retrofit.geofil.distributed.spark.util.MinLongAccumulator;
import org.apache.spark.SparkContext;
import org.apache.spark.util.CollectionAccumulator;
import scala.Tuple2;

import java.io.IOException;
import java.io.Serializable;
import java.net.*;
import java.util.*;

public class DynamicDistributor<K,V> implements Runnable, Serializable {

    private volatile boolean exit = false;

    public static final String USER_METRICS_NAMESPACE = "key-metrics";
    public static final String PRE_COUNT_METRIC_PREFIX = "Pre-Pub-count-";
    public static final String COUNT_METRIC_PREFIX = "Pub-count-";
    public static final String PUB_IP_METRIC_PREFIX = "Pub-ip-";
    public static final String PRE_PUB_IP_METRIC_PREFIX = "Pre-Pub-ip-";
    public static final String SUB_IP_METRIC_PREFIX = "Sub-ip-";
    public static final String COUNT_MS_METRIC_PREFIX = "Pub-duration-";
    public static final int MIN_NON_REPARTITION_PERIODS = 3;
    public static final double MIN_IMPROVEMENT_IN_DIFF_COEF = 0.3;

    private String webpage;
    private boolean toDynamicRepartition;
    private long period;
    private int numberOfPartitions;
    private int minNonRepartitionPeriods;
    private double diffCoefThresh;
    private PartitionMapper partitionMapper;
    private String metricsPrefix;
    private RepartitionableRDDHandler<K,V> repartitionableRDDHandler;
    private List<String> locations;
    private boolean toDataBalance;
    private MinLongAccumulator minTime;
    private CollectionAccumulator<Tuple2<Long, Double>> mLDCBySecond;
    private int lastRepartitionedBeforePeriods;
    public DynamicDistributor(String webpage, String toDynamicRepartition, long period, SparkContext sc, int numberOfPartitions, int minNonRepartitionPeriods, double diffCoefThresh,
                              RepartitionableRDDHandler<K, V> repartitionableRDDHandler, List<String> locations, boolean toDataBalance, MinLongAccumulator minTime, CollectionAccumulator<Tuple2<Long, Double>> mLDCBySecond){
        this.webpage = webpage;
        this.toDynamicRepartition = !toDynamicRepartition.equalsIgnoreCase("false");
        this.period = period;
        this.numberOfPartitions = numberOfPartitions;
        this.minNonRepartitionPeriods = minNonRepartitionPeriods;
        this.partitionMapper = new PartitionMapper(locations);
        this.metricsPrefix = sc.applicationId() + "." + sc.env().metricsSystem().instance() + "." + sc.appName();
        this.diffCoefThresh = diffCoefThresh;
        this.repartitionableRDDHandler = repartitionableRDDHandler;
        this.locations = locations;
        this.lastRepartitionedBeforePeriods = Integer.MAX_VALUE;
        this.toDataBalance = toDataBalance;
        this.minTime = minTime;
        this.mLDCBySecond = mLDCBySecond;
    }

    public void stop(){
        exit = true;
    }

    @Override
    public void run() {
        while(!exit) {
            try {
                for(int i = 0; i < 10; i++){
                    URL url = new URL(webpage);
                    HttpURLConnection http = (HttpURLConnection)url.openConnection();
                    http.setRequestProperty("Accept", "application/json");

                    ObjectMapper mapper = new ObjectMapper();

                    JsonNode node = mapper.readTree(http.getInputStream());

                    extractLocations(node);
                    extractKeyCounts(node);

                    System.out.println("PartitionMapper: " + partitionMapper);

                    Map<String, Long> countPerHostMap = partitionMapper.calculateCountPerHost();
                    System.out.println(countPerHostMap);

                    if(minTime.value() != null && minTime.value() != Long.MAX_VALUE) mLDCBySecond.add(new Tuple2<>((System.currentTimeMillis()-minTime.value())/1000, partitionMapper.calculateDifferenceCoef()));
                    if(exit) break;
                    Thread.sleep(period/10);
                }

                URL url = new URL(webpage);
                HttpURLConnection http = (HttpURLConnection)url.openConnection();
                http.setRequestProperty("Accept", "application/json");

                ObjectMapper mapper = new ObjectMapper();

                JsonNode node = mapper.readTree(http.getInputStream());

                extractLocations(node);
                extractKeyCounts(node);

                System.out.println("PartitionMapper: " + partitionMapper);

                Map<String, Long> countPerHostMap = partitionMapper.calculateCountPerHost();
                System.out.println(countPerHostMap);

                Double diffCoef = partitionMapper.calculateDifferenceCoef();
                System.out.println("Max diff coef: " + diffCoef);

                if(toDynamicRepartition && diffCoef > diffCoefThresh && lastRepartitionedBeforePeriods > minNonRepartitionPeriods){
                    repartition(node, diffCoef);
                }else increaseLastRepartitionedBeforePeriods();

                http.disconnect();
            }catch (InterruptedException | IOException e) {
                if(e instanceof ConnectException) break;
                e.printStackTrace();
            }
        }

    }

    public long getPeriod() {
        return period;
    }

    public static<K,V> DynamicDistributor<K,V> startDynamicRepartitionerThread(AbstractProcessor processor, int numberOfPartitions,
                                                                               RepartitionableRDDHandler<K ,V> repartitionableRDDHandler,
                                                                               List<String> locations, boolean toDataBalance, MinLongAccumulator minTime, CollectionAccumulator<Tuple2<Long, Double>> mLDCBySecond){
        DynamicDistributor dr = new DynamicDistributor(
                processor.cmd.getOptionValue("metrics-website-location", "http://manager.streamslab.fer.hr:4040/metrics/json/"),
                processor.cmd.getOptionValue("dynamic-repartition-boolean", "true"),
                Long.parseLong(processor.cmd.getOptionValue("dynamic-repartitioning-period", "30000")),
                processor.jsc.sc(), numberOfPartitions, Integer.parseInt(processor.cmd.getOptionValue("dynamic-min-non-repartition-periods", String.valueOf(MIN_NON_REPARTITION_PERIODS))), Double.parseDouble(processor.cmd.getOptionValue("difference-coef-threshold", "0.5"))
                ,repartitionableRDDHandler, locations, toDataBalance, minTime, mLDCBySecond);
        (new Thread(dr)).start();
        return dr;
    }

    private void increaseLastRepartitionedBeforePeriods(){
        if(lastRepartitionedBeforePeriods != Integer.MAX_VALUE){
            lastRepartitionedBeforePeriods++;
        }
    }

//    public void extractAndResetKeyCounts(JsonNode node){
//        int partitionId = 0;
//        do{
//            Long count = getLongFromCounter(node, metricsPrefix  + "." + USER_METRICS_NAMESPACE, COUNT_MS_METRIC_PREFIX + partitionId);
//            if(count != null) {
//                partitionMapper.setCount(partitionId, count);
//                if(count != 0) UserMetricsSystem.counter(COUNT_MS_METRIC_PREFIX + partitionId).dec(count);
//            }
//            partitionId++;
//        }while(partitionId<numberOfPartitions);
//    }

    public void extractKeyCounts(JsonNode node){
        int partitionId = 0;
        do{
            Long count = getLongFromCounter(node, metricsPrefix + "." + USER_METRICS_NAMESPACE, (toDataBalance ? COUNT_METRIC_PREFIX : COUNT_MS_METRIC_PREFIX) + partitionId);
            if(count != null) {
                partitionMapper.setCount(partitionId, count);
            }
            partitionId++;
        }while(partitionId<numberOfPartitions);
    }


    public void extractLocations(JsonNode node){
        int partitionId = 0;
        do{
            String host = getStringFromGauge(node, metricsPrefix + "." + USER_METRICS_NAMESPACE, "Sub-ip-" + partitionId);
            if(host != null) {
                partitionMapper.setLocation(partitionId, host);
            }
            partitionId++;
        }while(partitionId<numberOfPartitions);
    }

    public Integer extractWaitingBatches(JsonNode node){
        Integer waitingBatches;
        try{
            waitingBatches = Integer.valueOf(getStringFromGauge(node,
                    metricsPrefix + "." + "StreamingMetrics.streaming",
                    "waitingBatches"));
        }catch(NullPointerException ex){
            waitingBatches = null;
        }
        return waitingBatches;
    }

    public Integer extractUnprocessedBatches(JsonNode node){
        Integer waitingBatches;
        try{
            waitingBatches = Integer.valueOf(getStringFromGauge(node,
                    metricsPrefix + "." + "StreamingMetrics.streaming",
                    "unprocessedBatches"));
        }catch(NullPointerException ex){
            waitingBatches = null;
        }
        return waitingBatches;
    }

    public Integer extractProcessedRecords(JsonNode node){
        Integer waitingBatches;
        try{
            waitingBatches = Integer.valueOf(getStringFromGauge(node,
                    metricsPrefix + "." + "StreamingMetrics.streaming",
                    "totalProcessedRecords"));
        }catch(NullPointerException ex){
            waitingBatches = null;
        }
        return waitingBatches;
    }



    public static String getStringFromGauge(JsonNode node, String metricsPrefix, String metricName){
        try{
            return node.get("gauges").get(metricsPrefix + "." + metricName).get("value").asText();
        }catch(NullPointerException e){
            return null;
        }
    }

    public static Long getLongFromCounter(JsonNode node, String metricsPrefix, String metricName){
        try{
            return node.get("counters").get(metricsPrefix + "." + metricName).get("count").asLong();
        }catch(NullPointerException e){
            return null;
        }
    }

    public void repartition(JsonNode node, double diffCoef){
        System.out.println("Num of partitions: " + numberOfPartitions);
        PartitionMapper.RepartitionSuggestion repartitionSuggestion = partitionMapper.repartitionGreedy(locations.size());
        Map<Integer, String> repartitionSuggestionMap = repartitionSuggestion.getRepartitionMap();
        Double newDiffCoef = PartitionMapper.calculateDifferenceCoefFromCountsList(repartitionSuggestion.getLocationCounts());
        System.out.println("Suggested diff coef: " + newDiffCoef);
        Integer processedRecords = extractProcessedRecords(node);
        System.out.println("Processed records: " + processedRecords);
        System.out.println("#1: " + newDiffCoef);
        System.out.println("#2: " + diffCoef );
        System.out.println("#3: " + (diffCoef-newDiffCoef));
        Integer unprocessedBatches = extractUnprocessedBatches(node);

        if((newDiffCoef > diffCoef) || ((diffCoef-newDiffCoef) < MIN_IMPROVEMENT_IN_DIFF_COEF) || (processedRecords < 1100)){
            System.out.println("No repartition due to too small improvement in difference coefficient or sample records haven't been processed.");
            increaseLastRepartitionedBeforePeriods();
            return;
        }

        System.out.println("Repartition suggestion: " + repartitionSuggestionMap);

        lastRepartitionedBeforePeriods = 1;
        long startTime = System.currentTimeMillis();
        repartitionableRDDHandler.repartitionRDDandUpdateSubsLocations(repartitionSuggestionMap);
        long repartitioningTime = System.currentTimeMillis() - startTime;
        Integer waitingBatches = extractWaitingBatches(node);
        String toPrint = "REPARTITIONED!";
        toPrint += "[repartitioning time: " + repartitioningTime + "ms; waiting batches: " + waitingBatches + "; unprocessed batches: " + unprocessedBatches + "]";
        System.out.println(toPrint);
    }
}
