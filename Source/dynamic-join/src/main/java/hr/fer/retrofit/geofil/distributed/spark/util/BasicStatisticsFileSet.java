package hr.fer.retrofit.geofil.distributed.spark.util;

import hr.fer.retrofit.dynamic_join.geops_util.statistics.StatisticsFile;
import hr.fer.retrofit.dynamic_join.geops_util.statistics.StatisticsFileSet;
import hr.fer.retrofit.geofil.distributed.spark.AbstractProcessor;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class BasicStatisticsFileSet extends StatisticsFileSet {

    private BasicStatisticsFile basicStaticSTRLStatisticsFile;
    private BasicStatisticsFile basicStaticSTALStatisticsFile;
    private BasicStatisticsFile basicDynamicLB_STAL_StatisticsFile;
    private BasicStatisticsFile basicDynamicLB_STRL_StatisticsFile;
    private BasicStatisticsFile basicDynamicDB_STAL_StatisticsFile;
    private BasicStatisticsFile basicDynamicDB_STRL_StatisticsFile;

    public List<StatisticsFile> getAllFiles(){
        return Arrays.asList(basicStaticSTRLStatisticsFile, basicStaticSTALStatisticsFile, basicDynamicLB_STAL_StatisticsFile, basicDynamicLB_STRL_StatisticsFile,
                basicDynamicDB_STAL_StatisticsFile, basicDynamicDB_STRL_StatisticsFile);
    }

    public BasicStatisticsFile getBasicDynamicDB_STRL_StatisticsFile() {
        return basicDynamicDB_STRL_StatisticsFile;
    }

    public BasicStatisticsFile getBasicDynamicDB_STAL_StatisticsFile() {
        return basicDynamicDB_STAL_StatisticsFile;
    }

    public BasicStatisticsFileSet(Set<StatisticsFile> fileset){
        super(fileset);
        this.basicStaticSTRLStatisticsFile = (BasicStatisticsFile) staticSTRLStatisticsFile;
        this.basicDynamicLB_STAL_StatisticsFile = (BasicStatisticsFile) dynamicLB_STAL_StatisticsFile;
        this.basicDynamicLB_STRL_StatisticsFile = (BasicStatisticsFile) dynamicLB_STRL_StatisticsFile;
        this.basicStaticSTALStatisticsFile = (BasicStatisticsFile) staticSTALStatisticsFile;
        this.basicDynamicDB_STRL_StatisticsFile = (BasicStatisticsFile) dynamicDB_STRL_StatisticsFile;
        this.basicDynamicDB_STAL_StatisticsFile = (BasicStatisticsFile) dynamicDB_STAL_StatisticsFile;
    }

    public String getTypeString(BasicStatisticsFile file){
        if(file.getId().equals(basicStaticSTALStatisticsFile.getId())) return "Static STAL";
        if(file.getId().equals(basicStaticSTRLStatisticsFile.getId())) return "Static STRL";
        if(file.getId().equals(basicDynamicLB_STAL_StatisticsFile.getId())) return "Dynamic LB STAL";
        if(file.getId().equals(basicDynamicLB_STRL_StatisticsFile.getId())) return "Dynamic LB STRL";
        if(file.getId().equals(basicDynamicDB_STAL_StatisticsFile.getId())) return "Dynamic DB STAL";
        if(file.getId().equals(basicDynamicDB_STRL_StatisticsFile.getId())) return "Dynamic DB STRL";
        return null;
    }

    public void writeComparationToHdfs(){

        try ( PrintWriter pw = new PrintWriter(FileSystem.get(new URI("hdfs://primary:8020"), new Configuration()).create(new Path("hdfs://"
                + "primary:8020" + AbstractProcessor.HDFS_COMPARATION_RELATIVE_PATH + (comparationName!=null ? (comparationName + "-") : "") +
                getExpNums()
                        .stream().sorted(Integer::compareTo).collect(Collectors.toList()).toString().replace(" ", "").replace("[", "")
                        .replace("]", "")+ ".txt")))) {
            List<BasicStatisticsFile> filesList = getAllFiles().stream().map(file -> (BasicStatisticsFile) file).collect(Collectors.toList());
            filesList.forEach(file -> pw.write(getTypeString(file) + ": " + file.getId() + "\n"));
            filesList.forEach(file -> pw.write(getTypeString(file) + " throughput: " + String.format("%.2f", file.extractThroughput()) + " pubs/s" + "\n"));
            pw.write("Dynamic improvement in throughput: " + valuesToImprovementsString(basicDynamicLB_STAL_StatisticsFile.extractThroughput(),
                    filesList.stream().filter(file -> !file.equals(dynamicLB_STAL_StatisticsFile)).map(file -> file.extractThroughput()).collect(Collectors.toList()), true)
            + "\n");
        } catch (IOException | URISyntaxException ex) {
        }
    }

    public static String valuesToImprovementsString(double dynamicValues, List<Double> otherValues, boolean isThroughput){
        if(isThroughput){
            return otherValues.stream().map(otherThroughput -> String.format("%.0f", (dynamicValues/otherThroughput-1)*100)+"%").collect(Collectors.toList()).toString().replace(" ", "").replace("[", "").replace("]", "");
        }
        else{
            return otherValues.stream().map(otherLatency -> String.format("%.0f", -(dynamicValues/otherLatency-1)*100)+"%").collect(Collectors.toList()).toString().replace(" ", "").replace("[", "").replace("]", "");
        }
    }
    
    

    public double getStaticSTRLLatency() {
        return basicStaticSTRLStatisticsFile.extractLatency();
    }

    public double getStaticSTALLatency() {
        return basicStaticSTALStatisticsFile.extractLatency();
    }

    public double getDynamicLB_STAL_Latency() {
        return basicDynamicLB_STAL_StatisticsFile.extractLatency();
    }

    public double getStaticSTRLThroughput() {
        return basicStaticSTRLStatisticsFile.extractThroughput();
    }

    public double getStaticSTALThroughput() {
        return basicStaticSTALStatisticsFile.extractThroughput();
    }

    public double getDynamicLB_STAL_Throughput() {
        return basicDynamicLB_STAL_StatisticsFile.extractThroughput();
    }

    public double getDynamicLB_STRL_Throughput() {
        return basicDynamicLB_STRL_StatisticsFile.extractThroughput();
    }

    public double getDynamicLB_STRL_Latency() {
        return basicDynamicLB_STRL_StatisticsFile.extractLatency();
    }

    public double getDynamicDB_STAL_Latency() {
        return basicDynamicDB_STAL_StatisticsFile.extractLatency();
    }

    public double getDynamicDB_STAL_Throughput() {
        return basicDynamicDB_STAL_StatisticsFile.extractThroughput();
    }

    public double getDynamicDB_STRL_Latency() {
        return basicDynamicDB_STRL_StatisticsFile.extractLatency();
    }

    public double getDynamicDB_STRL_Throughput() {
        return basicDynamicDB_STRL_StatisticsFile.extractThroughput();
    }



//    private StatisticsFile extractDynamicStatisticsFile(Set<StatisticsFile> fileset){
//        return fileset.stream().filter(file -> file.getId().contains(DYNAMIC_PROCESSOR_CONTAINS) && !file.getId().contains(DYNAMIC_REPARTITION_FALSE))
//                .findFirst().get();
//    }
//
//    private StatisticsFile extractDynamicInactiveStatisticsFile(Set<StatisticsFile> fileset){
//        return fileset.stream().filter(file -> file.getId().contains(DYNAMIC_PROCESSOR_CONTAINS) && file.getId().contains(DYNAMIC_REPARTITION_FALSE))
//                .findFirst().get();
//    }
//
//    private StatisticsFile extractStaticStatisticsFile(Set<StatisticsFile> fileset){
//        return fileset.stream().filter(file -> !file.getId().contains(DYNAMIC_PROCESSOR_CONTAINS))
//                .findFirst().get();
//    }
//
//    public BasicStatisticsFile getDynamicStatisticsFile() {
//        return (BasicStatisticsFile) dynamicStatisticsFile;
//    }
//
//    public void setDynamicStatisticsFile(BasicStatisticsFile dynamicStatisticsFile) {
//        this.dynamicStatisticsFile = dynamicStatisticsFile;
//    }
//
//    public BasicStatisticsFile getDynamicInactiveStatisticsFile() {
//        return (BasicStatisticsFile) dynamicInactiveStatisticsFile;
//    }
//
//    public void setDynamicInactiveStatisticsFile(BasicStatisticsFile basicDynamicInactiveStatisticsFile) {
//        this.dynamicInactiveStatisticsFile = basicDynamicInactiveStatisticsFile;
//    }
//
//    public BasicStatisticsFile getStaticStatisticsFile() {
//        return (BasicStatisticsFile) staticStatisticsFile;
//    }
//
//    public void setStaticStatisticsFile(BasicStatisticsFile staticStatisticsFile) {
//        this.staticStatisticsFile = staticStatisticsFile;
//    }
}
