package hr.fer.retrofit.geofil.distributed.kafka;

import hr.fer.retrofit.geofil.data.model.Publication;
import hr.fer.retrofit.geofil.distributed.kafka.serde.ObjectDeserializer;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.LongDeserializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import scala.Tuple2;

import java.util.*;
import java.util.concurrent.Callable;

public class SimpleKafkaTopicConsumer implements Callable<List<Tuple2<Integer, Set<Integer>>>> {

    private Properties props;
    private Consumer<Publication, Set<Integer>> consumer;

    public SimpleKafkaTopicConsumer(String topic) {
        props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,
                "broker01:9092,broker02:9092,broker03:9092,broker04:9092");
        props.put(ConsumerConfig.GROUP_ID_CONFIG,
                "KafkaExampleConsumer");
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,
                ObjectDeserializer.class.getName());
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
                ObjectDeserializer.class.getName());
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "latest");

        // Create the consumer using props.
        consumer = new KafkaConsumer<>(props);

        // Subscribe to the topic.
        consumer.subscribe(Collections.singletonList(topic));
    }

    @Override
    public List<Tuple2<Integer, Set<Integer>>> call() throws Exception {
        final int giveUp = 3;
        int noRecordsCount = 0;

        List<Tuple2<Integer, Set<Integer>>> records = new ArrayList<>();

        while (true) {
            final ConsumerRecords<Publication, Set<Integer>> consumerRecords =
                    consumer.poll(30000L);

            if (consumerRecords.count() == 0) {
                noRecordsCount++;
                if (noRecordsCount > giveUp) break;
                else continue;
            }

            consumerRecords.forEach(record -> {
                records.add(new Tuple2<>(record.key().getId(), record.value()));
                System.out.println(new Tuple2<>(record.key().getId(), record.value()));
            });
            consumer.commitAsync();
        }
        consumer.close();
        return records;

    }
}
