package hr.fer.retrofit.geofil.distributed.spark;

import com.carrotsearch.sizeof.RamUsageEstimator;
import hr.fer.retrofit.geofil.distributed.kafka.serde.ObjectDeserializer;
import hr.fer.retrofit.geofil.distributed.kafka.serde.ObjectSerializer;
import hr.fer.retrofit.dynamic_join.distributor.DynamicDistributor;
import hr.fer.retrofit.geofil.distributed.spark.util.MaxLongAccumulator;
import hr.fer.retrofit.geofil.distributed.spark.util.MinLongAccumulator;
import hr.fer.retrofit.geofil.distributed.spark.util.StatsCollectorStreamingListener;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.Duration;
import java.util.*;
import java.util.stream.Collectors;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.kafka.common.serialization.IntegerDeserializer;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.util.CollectionAccumulator;
import org.apache.spark.util.LongAccumulator;
import scala.Tuple2;
import scala.Tuple3;

public abstract class AbstractProcessor {

    public CommandLine cmd;
    protected final Map<String, Object> kafkaParams;
    protected final Collection<String> topics;
    protected final Logger logger;
    protected final String hdfsHostPort;

    protected final JavaStreamingContext jssc;
    protected final SparkConf sparkConf;
    public final JavaSparkContext jsc;

    public static long THROUGHPUT_LATENCY_PERIOD_SECS = 5L;

    protected final LongAccumulator numRecords;
    protected final LongAccumulator processingDelay;
    protected final LongAccumulator firstKey;
    protected final CollectionAccumulator<Tuple3<Long, Long, Long>> throughputLatencyBySecond;
    protected final CollectionAccumulator<Tuple2<Long, Double>> mLDCBySecond;
    protected final MaxLongAccumulator maxTime;
    protected final MinLongAccumulator minTime;

    protected final Properties producerProps;
    
    protected final int numberOfRecordsToProcess;

    private static final String DEFAULT_BROKERS = "broker01:9092,broker02:9092,broker03:9092,broker04:9092";
    public static final String HDFS_EXPERIMENT_RELATIVE_PATH = "/user/jmaric/geofil/experiment-";
    public static final String HDFS_THROUGHPUT_RELATIVE_PATH = "/user/jmaric/geofil/experiment-throughput-";
    public static final String HDFS_MLDC_RELATIVE_PATH = "/user/jmaric/geofil/experiment-mldc-";
    public static final String HDFS_LATENCY_RELATIVE_PATH = "/user/jmaric/geofil/experiment-latency-";
    public static final String HDFS_COMPARATION_RELATIVE_PATH = "/user/jmaric/geofil/comparation-";
    public static final String HDFS_THROUGHPUTS_RELATIVE_PATH = "/user/jmaric/geofil/throughputs-";
    public static final String HDFS_LATENCIES_RELATIVE_PATH = "/user/jmaric/geofil/latencies-";
    public static final String HDFS_AVG_THROUGHPUTS_RELATIVE_PATH = "/user/jmaric/geofil/avg-throughputs-";
    public static final String HDFS_AVG_LATENCIES_RELATIVE_PATH = "/user/jmaric/geofil/avg-latencies-";

    public static final String STREAM_LOCALITY_JOIN_OPTION_NAME = "stream-locality-join";
    public static final String TO_DATA_BALANCE_OPTION_NAME = "to-data-balance";


    public AbstractProcessor(String[] args) {
        Options options = parseArgs(args);
        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("command", options);
            System.exit(1);
        }

        logger = Logger.getLogger(this.getClass());
        setLoggerLevel();

        logger.info(getAppName(this));
        System.out.println(getAppName(this));

        sparkConf = new SparkConf().setAppName(getAppName(this));
        initializeSparkConf();
        hdfsHostPort = sparkConf.get("spark.master").contains("local") ? "primary:8020" : "";

        kafkaParams = new HashMap<>();
        initializeKafkaParams();
        topics = Arrays.asList("geofil-" + cmd.getOptionValue("number-of-publications"));

        //initialize spark context
        jssc = new JavaStreamingContext(sparkConf, Durations.milliseconds(500L));
        jsc = jssc.sparkContext();
        numRecords = jsc.sc().longAccumulator();
        processingDelay = jsc.sc().longAccumulator();
        firstKey = jsc.sc().longAccumulator();
        throughputLatencyBySecond = jsc.sc().collectionAccumulator();
        mLDCBySecond = jsc.sc().collectionAccumulator();
        maxTime = new MaxLongAccumulator();
        jsc.sc().register(maxTime);
        minTime = new MinLongAccumulator();
        jsc.sc().register(minTime);
        jssc.addStreamingListener(new StatsCollectorStreamingListener(numRecords, processingDelay, minTime, maxTime, throughputLatencyBySecond));

        producerProps = new Properties();
        producerProps.put("bootstrap.servers", cmd.getOptionValue("bootstrap.servers", DEFAULT_BROKERS));
        producerProps.put("key.serializer", ObjectSerializer.class.getName());
        producerProps.put("value.serializer", ObjectSerializer.class.getName());
        
        if (!cmd.hasOption("number-of-records-to-process")) {
            System.out.println("Number of records to process has to be defined");
            System.exit(1);
        }
        numberOfRecordsToProcess = Integer.parseInt(cmd.getOptionValue("number-of-records-to-process"));
    }

    protected void addLoggingShutdownHook(int exactNumberOfPartitions, long sizeOfSubscriptions, List<Long> repartitionTimes){
        final int finalExperimentNo = Integer.parseInt(cmd.getOptionValue("number-of-experiment"));
        final String finalAppName = getAppName(this, exactNumberOfPartitions);
        final JavaSparkContext finaljsc = jsc;
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                logger.info("Shutting down streaming app...");

                //print statistics
                final long processingTime = maxTime.value() - minTime.value();
                logger.info(finalAppName);
                logger.info("Number of processed records: " + numRecords.value());
                logger.info("Cumulative processing delay in milliseconds: " + processingDelay.value());
                logger.info("Average processing latency in ms: " + (processingDelay.value() / (double) numRecords.value()));
                logger.info("Processing time in ms: " + processingTime);
                logger.info("Processing throughput in pubs/s: " + 1000 * ((double) numRecords.value()) / processingTime);
                logger.info("Min time in ms: " + minTime.value());

                logger.info("Subscriptions size: " + RamUsageEstimator.humanReadableUnits(sizeOfSubscriptions));
                if(repartitionTimes != null && !repartitionTimes.isEmpty()) logger.info("First repartitioning time in ms: " + repartitionTimes.get(0) + "\n");



                //write statistics to hdfs
                try ( PrintWriter pw = new PrintWriter(FileSystem.get(jsc.hadoopConfiguration()).create(new Path("hdfs://"
                        + hdfsHostPort + AbstractProcessor.HDFS_EXPERIMENT_RELATIVE_PATH + finalExperimentNo + ".txt")))) {
                    pw.write(finalAppName + "\n");
                    pw.write("Number of processed records: " + numRecords.value() + "\n");
                    pw.write("Cumulative processing delay in milliseconds: " + processingDelay.value() + "\n");
                    pw.write("Average processing latency in ms:" + (processingDelay.value() / (double) numRecords.value()) + "\n");
                    pw.write("Processing time in ms: " + processingTime + "\n");
                    pw.write("Processing throughput in pubs/s: " + 1000 * ((double) numRecords.value() / processingTime) + "\n");

                    pw.write("Subscriptions size: " + RamUsageEstimator.humanReadableUnits(sizeOfSubscriptions) + "\n");
                    pw.write("App name and ID: " + finalAppName + " " + finaljsc.sc().applicationId() + "\n");
                    if(repartitionTimes != null && !repartitionTimes.isEmpty()) pw.write("First repartitioning time in ms: " + repartitionTimes.get(0) + "\n");

                    pw.write("Fine print latency: " + finalAppName + " " + (processingDelay.value() / (double) numRecords.value()) + "\n");
                    pw.write("Fine print throughput: " + finalAppName + " " + 1000 * ((double) numRecords.value() / processingTime) + "\n");
                    pw.flush();
                } catch (Exception ex) {
                    logger.info(("Cannot store statistics to HDFS: " + ex.getStackTrace().toString()));
                }

                List<Tuple2<Long, Double>> mLDCBySecondTuples = mLDCBySecond.value().stream().sorted(Comparator.comparingLong(t -> t._1)).collect(Collectors.toList());

                try ( PrintWriter pw = new PrintWriter(FileSystem.get(jsc.hadoopConfiguration()).create(new Path("hdfs://"
                        + hdfsHostPort + AbstractProcessor.HDFS_MLDC_RELATIVE_PATH + finalExperimentNo + ".txt")))) {

                    pw.write(finalAppName + "\n");

                    mLDCBySecondTuples
                            .forEach(t -> pw.write(t._1 + "," + String.format("%.2f", t._2) + "\n"));

                    pw.flush();
                } catch (IOException ex) {
                    logger.info("Cannot store statistics to HDFS");
                }


                Map<Long, Double> numRecordsByPeriod = throughputLatencyBySecond.value().stream().collect(
                        Collectors.groupingBy(t -> (t._1()/ THROUGHPUT_LATENCY_PERIOD_SECS +1)* THROUGHPUT_LATENCY_PERIOD_SECS, Collectors.summingDouble(t -> t._2())));
                try ( PrintWriter pw = new PrintWriter(FileSystem.get(jsc.hadoopConfiguration()).create(new Path("hdfs://"
                        + hdfsHostPort + AbstractProcessor.HDFS_EXPERIMENT_RELATIVE_PATH + "throughput-" + finalExperimentNo + ".txt")))) {

                    pw.write(finalAppName + "\n");

                    numRecordsByPeriod
                            .entrySet().stream().map(e -> new Tuple2<>(e.getKey(), e.getValue()/ THROUGHPUT_LATENCY_PERIOD_SECS))
                            .sorted(Comparator.comparingLong(t -> t._1))
                            .forEach(t -> pw.write(t._1 + "," + String.format("%.2f", t._2) + "\n"));

                    pw.flush();
                } catch (IOException ex) {
                    logger.info("Cannot store statistics to HDFS");
                }

                try ( PrintWriter pw = new PrintWriter(FileSystem.get(jsc.hadoopConfiguration()).create(new Path("hdfs://"
                        + hdfsHostPort + AbstractProcessor.HDFS_EXPERIMENT_RELATIVE_PATH + "latency-" + finalExperimentNo + ".txt")))) {

                    pw.write(finalAppName + "\n");

                    throughputLatencyBySecond.value().stream().collect(Collectors.groupingBy(t -> (t._1()/ THROUGHPUT_LATENCY_PERIOD_SECS +1)* THROUGHPUT_LATENCY_PERIOD_SECS, Collectors.summingDouble(t -> t._3())))
                            .entrySet().stream().map(e -> new Tuple2<>(e.getKey(), e.getValue()/numRecordsByPeriod.get(e.getKey())))
                            .sorted(Comparator.comparingLong(t -> t._1))
                            .forEach(t -> pw.write(t._1 + "," + String.format("%.2f", t._2) + "\n"));

                    pw.flush();
                } catch (IOException ex) {
                    logger.info("Cannot store statistics to HDFS");
                }

                logger.info("Shutdown of streaming app complete.");
            }
        });

    }

    //this is required for slow strategies
    protected void waitUntilAllConsumed(AbstractProcessor processor) {
        try {

//            long prevNumRecords = 0;
//
//            do {
//                prevNumRecords = processor.numRecords.value();
//                Thread.sleep(Duration.ofMinutes(10).getSeconds() * 1000);
//            } while (prevNumRecords < processor.numRecords.value());

            //processed records - 9859
            do {
                Thread.sleep(Duration.ofSeconds(20).getSeconds() * 1000);
                System.out.println("Processed records: " + processor.numRecords.value());
            } while (processor.numRecords.value() < numberOfRecordsToProcess);


            logger.info("Kafka stream empty");
            processor.jssc.stop(true, false);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

    protected void waitUntilAllConsumed(AbstractProcessor processor, DynamicDistributor dr) {
        try {

//            long prevNumRecords = 0;
//
//            do {
//                prevNumRecords = processor.numRecords.value();
//                Thread.sleep(Duration.ofMinutes(10).getSeconds() * 1000);
//            } while (prevNumRecords < processor.numRecords.value());

            //processed records - 9859
            do {
                Thread.sleep(Duration.ofSeconds(15).getSeconds() * 1000);
                System.out.println("Processed records: " + processor.numRecords.value());
            } while (processor.numRecords.value() < numberOfRecordsToProcess);


            logger.info("Kafka stream empty");
            if(dr != null) dr.stop();
            Thread.sleep((dr.getPeriod()/10) + 1000L);
            processor.jssc.stop(true, false);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

    private void initializeSparkConf() {
        switch (cmd.getOptionValue("type-of-executors")) {
            case "local-custom":
                logger.info("local-custom executors...");
                sparkConf.set("spark.executor.cores", "2");
                sparkConf.set("spark.executor.instances", "2");
                sparkConf.set("spark.executor.memory", "6G");
                break;
            case "tiny":
                logger.info("tiny executors...");
                sparkConf.set("spark.executor.cores", "1");
                sparkConf.set("spark.executor.instances", "96");
                sparkConf.set("spark.executor.memory", "8G");
                break;
            case "small":
                logger.info("small executors...");
                sparkConf.set("spark.executor.cores", "2");
                sparkConf.set("spark.executor.instances", "48");
                sparkConf.set("spark.executor.memory", "16G");
                break;
            case "balanced":
                logger.info("balanced 32 executors...");
                sparkConf.set("spark.executor.cores", "3");
                sparkConf.set("spark.executor.instances", "32");
                sparkConf.set("spark.executor.memory", "24G");
                break;
            case "balanced16":
                logger.info("balanced 16 executors...");
                sparkConf.set("spark.executor.cores", "3");
                sparkConf.set("spark.executor.instances", "16");
                sparkConf.set("spark.executor.memory", "24G");
                break;
            case "balanced8":
                logger.info("balanced 8 executors...");
                sparkConf.set("spark.executor.cores", "3");
                sparkConf.set("spark.executor.instances", "8");
                sparkConf.set("spark.executor.memory", "24G");
                break;
            case "balanced4":
                logger.info("balanced 4 executors...");
                sparkConf.set("spark.executor.cores", "3");
                sparkConf.set("spark.executor.instances", "4");
                sparkConf.set("spark.executor.memory", "24G");
                break;
            case "balanced2":
                logger.info("balanced 2 executors...");
                sparkConf.set("spark.executor.cores", "3");
                sparkConf.set("spark.executor.instances", "2");
                sparkConf.set("spark.executor.memory", "24G");
                break;
            case "balanced1":
                logger.info("balanced 1 executors...");
                sparkConf.set("spark.executor.cores", "3");
                sparkConf.set("spark.executor.instances", "1");
                sparkConf.set("spark.executor.memory", "24G");
                break;
            case "fat":
                logger.info("fat executors...");
                sparkConf.set("spark.executor.cores", "6");
                sparkConf.set("spark.executor.instances", "16");
                sparkConf.set("spark.executor.memory", "48G");
                break;
            case "fat8":
                logger.info("fat 8 executors...");
                sparkConf.set("spark.executor.cores", "6");
                sparkConf.set("spark.executor.instances", "8");
                sparkConf.set("spark.executor.memory", "48G");
                break;
            case "fat4":
                logger.info("fat 4 executors...");
                sparkConf.set("spark.executor.cores", "6");
                sparkConf.set("spark.executor.instances", "4");
                sparkConf.set("spark.executor.memory", "48G");
                break;
            case "fat2":
                logger.info("fat 2 executors...");
                sparkConf.set("spark.executor.cores", "6");
                sparkConf.set("spark.executor.instances", "2");
                sparkConf.set("spark.executor.memory", "48G");
                break;
            case "fat1":
                logger.info("fat 1 executors...");
                sparkConf.set("spark.executor.cores", "6");
                sparkConf.set("spark.executor.instances", "1");
                sparkConf.set("spark.executor.memory", "48G");
                break;
            case "oversized":
                logger.info("oversized executors...");
                sparkConf.set("spark.executor.cores", "8");
                sparkConf.set("spark.executor.instances", "16");
                sparkConf.set("spark.executor.memory", "48G");
                break;
//            case "oversized-cro-ngi":
//                logger.info("fat cro-ngi executors...");
//                sparkConf.set("spark.executor.cores", "16");
//                sparkConf.set("spark.executor.instances", "9");
//                sparkConf.set("spark.executor.memory", "16G");
//                break;
        }

        sparkConf.set("spark.streaming.stopGracefullyOnShutdown", "true");
        sparkConf.set("spark.rpc.message.maxSize", "1024");
        if (cmd.hasOption("number-of-concurrent-jobs")) {
            sparkConf.set("spark.streaming.concurrentJobs", cmd.getOptionValue("number-of-concurrent-jobs"));
        }
        if (cmd.hasOption("max-rate-per-partition")) {
            sparkConf.set("spark.streaming.kafka.maxRatePerPartition", cmd.getOptionValue("max-rate-per-partition"));
        }
        if(cmd.hasOption("spark.locality.wait")){
            sparkConf.set("spark.locality.wait", cmd.getOptionValue("spark.locality.wait"));
        }
        if(cmd.hasOption("spark.streaming.backpressure.pid.minRate")){
            sparkConf.set("spark.streaming.backpressure.pid.minRate", cmd.getOptionValue("spark.streaming.backpressure.pid.minRate"));
        }
        if(cmd.hasOption("spark.streaming.backpressure.enabled")){
            sparkConf.set("spark.streaming.backpressure.enabled", cmd.getOptionValue("spark.streaming.backpressure.enabled"));
        }
        //set the master if not already set through the command line
        try {
            sparkConf.get("spark.master");
        } catch (NoSuchElementException ex) {
            sparkConf.setMaster("local[*]");
        }
    }

    private void setLoggerLevel() {
        Logger.getLogger("org").setLevel(Level.WARN);
        Logger.getLogger("akka").setLevel(Level.WARN);
    }

    protected static final String getAppName(AbstractProcessor processor) {
        StringBuilder sb = new StringBuilder(processor.getClass().getSimpleName());
        sb.append("-");
        for (Option option : processor.cmd.getOptions()) {
            if (option.hasArg()) {
                sb.append(option.getOpt());
                sb.append(":");
                sb.append(processor.cmd.getOptionValue(option.getLongOpt()));
                sb.append(",");
            } else {
                sb.append(option.getOpt());
                sb.append(":");
                sb.append(processor.cmd.hasOption(option.getOpt()));
                sb.append(",");
            }
        }
        return removeLastChar(sb.toString());
    }

    protected static final String getAppName(AbstractProcessor processor, int exactNumberOfPartitions) {
        StringBuilder sb = new StringBuilder(processor.getClass().getSimpleName());
        sb.append("-");
        for (Option option : processor.cmd.getOptions()) {
            if (option.hasArg()) {
                sb.append(option.getOpt());
                sb.append(":");
                if (option.getLongOpt().equals("number-of-partitions")) {
                    sb.append(exactNumberOfPartitions);
                } else {
                    sb.append(processor.cmd.getOptionValue(option.getLongOpt()));
                }
                sb.append(",");
            } else {
                sb.append(option.getOpt());
                sb.append(":");
                sb.append(processor.cmd.hasOption(option.getOpt()));
                sb.append(",");
            }
        }
        return removeLastChar(sb.toString());
    }

    public static String removeLastChar(String str) {
        return removeLastChars(str, 1);
    }

    public static String removeLastChars(String str, int chars) {
        return str.substring(0, str.length() - chars);
    }

    private void initializeKafkaParams() {

        kafkaParams.put("bootstrap.servers", cmd.getOptionValue("bootstrap.servers", "broker01:9092,broker02:9092,broker03:9092,broker04:9092"));
        kafkaParams.put("key.deserializer", IntegerDeserializer.class.getName());
        kafkaParams.put("value.deserializer", ObjectDeserializer.class.getName());
        kafkaParams.put("group.id", "group-" + cmd.getOptionValue("number-of-experiment"));
        kafkaParams.put("auto.offset.reset", "earliest");
        kafkaParams.put("enable.auto.commit", false);
        kafkaParams.put("max.poll.interval.ms", "1800000"); //set to 30 minutes instead of default 5 minutes since Sedona is to slow
    }

    private Options parseArgs(String[] args) {
        Options options = new Options();

        Option option = new Option("nex", "number-of-experiment", true, "the number of this experiment");
        option.setRequired(true);
        options.addOption(option);

        option = new Option("nsb", "number-of-subscriptions", true, "the number of subscriptions in this experiment");
        option.setRequired(true);
        options.addOption(option);

        option = new Option("npb", "number-of-publications", true, "the number of publications in this experiment");
        option.setRequired(true);
        options.addOption(option);

        option = new Option("tex", "type-of-executors", true, "the type of executors used in this experiment");
        option.setRequired(true);
        options.addOption(option);

        option = new Option("tix", "type-of-index", true, "the type of index used in this experiment");
        option.setRequired(false);
        options.addOption(option);

        option = new Option("tgd", "type-of-grid", true, "the type of grid used in this experiment");
        option.setRequired(false);
        options.addOption(option);

        option = new Option("npa", "number-of-partitions", true, "the number of subscription partitions in this experiment");
        option.setRequired(false);
        options.addOption(option);

        option = new Option("tso", "type-of-spatial-operator", true, "the type of spatial operator used in this experiment");
        option.setRequired(false);
        options.addOption(option);

        option = new Option("spc", "skip-processing", false, "skip the processing of data stream");
        option.setRequired(false);
        options.addOption(option);

        option = new Option("ncj", "number-of-concurrent-jobs", true, "the number of cuncurrent jobs");
        option.setRequired(true);
        options.addOption(option);
        
        option = new Option("nrp", "number-of-records-to-process", true, "the number of records to process");
        option.setRequired(true);
        options.addOption(option);
        
        option = new Option("mrp", "max-rate-per-partition", true, "max rate per Kafka partition");
        option.setRequired(true);
        options.addOption(option);

        option = new Option("bs", "bootstrap.servers", true, "kafka brokers");
        option.setRequired(false);
        options.addOption(option);

        option = new Option("stp", "statistics-prefix-path", true, "path to statistics");
        option.setRequired(false);
        options.addOption(option);

        option = new Option("sup", "subscriptions-prefix-path", true, "path to subscriptions");
        option.setRequired(false);
        options.addOption(option);

        option = new Option("mwl", "metrics-website-location", true, "website of metrics");
        option.setRequired(false);
        options.addOption(option);

        option = new Option("drb", "dynamic-repartition-boolean", true, "to execute repartition boolean");
        option.setRequired(false);
        options.addOption(option);

        option = new Option("strl", STREAM_LOCALITY_JOIN_OPTION_NAME, true, "to perform join with stream locality");
        option.setRequired(false);
        options.addOption(option);

        option = new Option("tdb", TO_DATA_BALANCE_OPTION_NAME, true, "to data balance(contrary to load balance)");
        option.setRequired(false);
        options.addOption(option);

        option = new Option("drp", "dynamic-repartitioning-period", true, "dynamic repartitioning period in seconds");
        option.setRequired(false);
        options.addOption(option);

        option = new Option("dct", "difference-coef-threshold", true, "max difference coefficient to repartition");
        option.setRequired(false);
        options.addOption(option);

        option = new Option("mnrp", "dynamic-min-non-repartition-periods", true, "dynamic min non repartition periods");
        option.setRequired(false);
        options.addOption(option);


//        if(cmd.hasOption("spark.streaming.backpressure.enabled")){
//        sparkConf.set("spark.streaming.backpressure.enabled", cmd.getOptionValue("spark.streaming.backpressure.enabled"));
//    }
//        if(cmd.hasOption("spark.streaming.backpressure.pid.minRate")){
//        sparkConf.set("spark.streaming.backpressure.pid.minRate", cmd.getOptionValue("spark.streaming.backpressure.pid.minRate

        option = new Option("slw", "spark.locality.wait", true, "spark.locality.wait configuration parameter");
        option.setRequired(false);
        options.addOption(option);

        option = new Option("ssbe", "spark.streaming.backpressure.enabled", true, "spark.streaming.backpressure.enabled configuration parameter");
        option.setRequired(false);
        options.addOption(option);

        option = new Option("ssbpm", "spark.streaming.backpressure.pid.minRate", true, "spark.streaming.backpressure.pid.minRate configuration parameter");
        option.setRequired(false);
        options.addOption(option);

        return options;
    }
}
