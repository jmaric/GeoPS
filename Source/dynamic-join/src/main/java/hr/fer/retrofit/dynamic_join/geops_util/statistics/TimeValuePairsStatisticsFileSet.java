package hr.fer.retrofit.dynamic_join.geops_util.statistics;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import scala.Tuple2;
import scala.Tuple7;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;
import java.util.stream.Collectors;

public class TimeValuePairsStatisticsFileSet extends StatisticsFileSet{

    private List<Tuple7<Integer, Double, Double, Double, Double, Double, Double>> timeValues;

    public TimeValuePairsStatisticsFileSet(Set<StatisticsFile> fileset) {
        super(fileset);
        readTimeValues();
    }

    public void writeCollectiveTimeValuePairsToHdfs(String hdfsRelativePath){
        writeCollectiveTimeValuePairsToHdfs(null, hdfsRelativePath, timeValues, getExpNums());
    }

    public List<Tuple7<Integer, Double, Double, Double, Double, Double, Double>> getTimeValues() {
        return timeValues;
    }

    private void readTimeValues(){
        Map<Integer, Double> staticSTALPairs = ((TimeValuePairsStatisticsFile) staticSTALStatisticsFile).readTimeValuePairs();
        Map<Integer, Double> staticSTRLPairs = ((TimeValuePairsStatisticsFile) staticSTRLStatisticsFile).readTimeValuePairs();
        Map<Integer, Double> dynamicLB_STAL_Pairs = ((TimeValuePairsStatisticsFile) dynamicLB_STAL_StatisticsFile).readTimeValuePairs();
        Map<Integer, Double> dynamicLB_STRL_Pairs = ((TimeValuePairsStatisticsFile) dynamicLB_STRL_StatisticsFile).readTimeValuePairs();
        Map<Integer, Double> dynamicDB_STAL_Pairs = ((TimeValuePairsStatisticsFile) dynamicDB_STAL_StatisticsFile).readTimeValuePairs();
        Map<Integer, Double> dynamicDB_STRL_Pairs = ((TimeValuePairsStatisticsFile) dynamicDB_STRL_StatisticsFile).readTimeValuePairs();

        timeValues = new ArrayList<>();
        for(Tuple2<Integer, Double> tuple1 : staticSTALPairs.entrySet().stream().map(e -> new Tuple2<>(e.getKey(), e.getValue()))
                .sorted(Comparator.comparingLong(t -> t._1)).collect(Collectors.toList())){
            Integer time = tuple1._1;
            Double value2 = staticSTRLPairs.get(time);
            Double value3 = dynamicLB_STAL_Pairs.get(time);
            Double value4 = dynamicLB_STRL_Pairs.get(time);
            Double value5 = dynamicDB_STAL_Pairs.get(time);
            Double value6 = dynamicDB_STRL_Pairs.get(time);

            if(value2 == null || value3 == null || value4 == null || value5 == null || value6 == null) continue;
            timeValues.add(new Tuple7<>(time, tuple1._2(), value2, value3, value4, value5, value6));
        }
        //            timeValues.add(0, new Tuple4<>(0, 0D, 0D, 0D));
        if(timeValues.size() > 0) timeValues.remove(timeValues.size()-1);
    }

    private void deleteOldTimeValuePairStatisticsFiles() {
        try {
            FileSystem fs = FileSystem.get(new URI("hdfs://primary:8020"), new Configuration());
            Arrays.asList(staticSTRLStatisticsFile, dynamicLB_STAL_StatisticsFile, staticSTALStatisticsFile).forEach(
                    statisticsFile -> {
                        try {
                            fs.delete(new Path(statisticsFile.getFilePath()), true);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
            );
            fs.close();
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public static void writeCollectiveTimeValuePairsToHdfs(String comparationName, String hdfsRelativePath,
                                                           List<Tuple7<Integer, Double, Double, Double, Double, Double, Double>> timeValues,
                                                           List<Integer> expNumbers){
        try ( PrintWriter pw = new PrintWriter(FileSystem.get(new URI("hdfs://primary:8020"), new Configuration()).create(new Path("hdfs://"
                + "primary:8020" + hdfsRelativePath + (comparationName!=null ? (comparationName + "-") : "") +
                expNumbers
                        .stream().sorted(Integer::compareTo).collect(Collectors.toList()).toString().replace(" ", "").replace("[", "")
                        .replace("]", "")+ ".txt")))) {

            timeValues
                    .forEach(t -> pw.write(t._1() + "," + String.format("%.2f", t._2()) + ","
                            + String.format("%.2f", t._3()) + "," + String.format("%.2f", t._4()) + ","
                            + String.format("%.2f", t._5()) + "," + String.format("%.2f", t._6()) + ","
                            + String.format("%.2f", t._7()) +"\n"));
            pw.flush();

        } catch (IOException | URISyntaxException ex) {
        }
    }

}
