package hr.fer.retrofit.dynamic_join.geops_util.statistics;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public abstract class StatisticsFileSet {
    protected StatisticsFile staticSTALStatisticsFile;
    protected StatisticsFile staticSTRLStatisticsFile;
    protected StatisticsFile dynamicLB_STAL_StatisticsFile;
    protected StatisticsFile dynamicLB_STRL_StatisticsFile;
    protected StatisticsFile dynamicDB_STAL_StatisticsFile;
    protected StatisticsFile dynamicDB_STRL_StatisticsFile;
    protected String comparationName = null;

    private static final String DYNAMIC_PROCESSOR_CONTAINS = "Dynamic";
    private static final String DYNAMIC_REPARTITION_FALSE = "drb:false";
    private static final String TDB_CONTAINS = "tdb:true";
    private static final String STRL_CONTAINS = "strl:true";

    public StatisticsFileSet(Set<StatisticsFile> fileset){
        this.staticSTRLStatisticsFile = extractStaticSTRLStatisticsFile(fileset);
        this.staticSTALStatisticsFile = extractStaticSTALStatisticsFile(fileset);
        this.dynamicLB_STAL_StatisticsFile = extractDynamicLB_STAL_StatisticsFile(fileset);
        this.dynamicLB_STRL_StatisticsFile = extractDynamicLB_STRL_StatisticsFile(fileset);
        this.dynamicDB_STAL_StatisticsFile = extractDynamic_DB_STAL_StatisticsFile(fileset);
        this.dynamicDB_STRL_StatisticsFile = extractDynamic_DB_STRL_StatisticsFile(fileset);
    }

    public List<StatisticsFile> getAllFiles(){
        return Arrays.asList(dynamicLB_STAL_StatisticsFile, staticSTALStatisticsFile, staticSTRLStatisticsFile, dynamicDB_STAL_StatisticsFile, dynamicDB_STRL_StatisticsFile);
    }

    public List<Integer> getExpNums(){
        return getAllFiles().stream().map(file -> file.extractExperimentNumber()).collect(Collectors.toList());
    }

    private StatisticsFile extractStaticSTALStatisticsFile(Set<StatisticsFile> fileset){
        return fileset.stream().filter(file -> file.getId().contains(DYNAMIC_PROCESSOR_CONTAINS) && file.getId().contains(DYNAMIC_REPARTITION_FALSE)
        && !file.isStreamLocalityJoin() && !file.getId().contains(TDB_CONTAINS))
                .findFirst().get();
    }

    private StatisticsFile extractStaticSTRLStatisticsFile(Set<StatisticsFile> fileset){
        return fileset.stream().filter(file -> file.isStatic())
                .findFirst().get();
    }

    private StatisticsFile extractDynamicLB_STAL_StatisticsFile(Set<StatisticsFile> fileset){
        return fileset.stream().filter(file -> file.isDynamic() && !file.isDataBalancing() && !file.isStreamLocalityJoin())
                .findFirst().get();
    }

    private StatisticsFile extractDynamicLB_STRL_StatisticsFile(Set<StatisticsFile> fileset){
        return fileset.stream().filter(file -> file.isDynamic() && !file.isDataBalancing() && file.isStreamLocalityJoin())
                .findFirst().get();
    }

    private StatisticsFile extractDynamic_DB_STRL_StatisticsFile(Set<StatisticsFile> fileset){
        return fileset.stream().filter(file -> file.getId().contains(DYNAMIC_PROCESSOR_CONTAINS) && file.getId().contains(TDB_CONTAINS) && file.getId().contains(STRL_CONTAINS))
                .findFirst().get();
    }

    private StatisticsFile extractDynamic_DB_STAL_StatisticsFile(Set<StatisticsFile> fileset){
        return fileset.stream().filter(file -> file.getId().contains(DYNAMIC_PROCESSOR_CONTAINS) && file.getId().contains(TDB_CONTAINS) && !file.getId().contains(STRL_CONTAINS))
                .findFirst().get();
    }



    public String getComparationName() {
        return comparationName;
    }

    public void setComparationName(String comparationName) {
        this.comparationName = comparationName;
    }

}
