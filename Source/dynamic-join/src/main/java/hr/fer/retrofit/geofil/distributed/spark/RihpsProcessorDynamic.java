package hr.fer.retrofit.geofil.distributed.spark;

import com.carrotsearch.sizeof.RamUsageEstimator;
import hr.fer.retrofit.geofil.data.indexing.PartitionedSubscriptionMapFactory;
import hr.fer.retrofit.geofil.data.indexing.SpatialIndexFactory;
import hr.fer.retrofit.geofil.data.indexing.SpatialIndexFactory.IndexType;
import hr.fer.retrofit.geofil.data.model.Publication;
import hr.fer.retrofit.geofil.data.model.Subscription;
import hr.fer.retrofit.dynamic_join.geops_util.monitoring.UserMetricsUtil;
import hr.fer.retrofit.dynamic_join.distributor.DynamicDistributor;
import hr.fer.retrofit.dynamic_join.distributor.RepartitionableRDDHandler;
import hr.fer.retrofit.geofil.distributed.spark.util.HostsUtil;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.spark.Partitioner;
import org.apache.spark.TaskContext;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.broadcast.Broadcast;
import org.apache.spark.streaming.api.java.JavaInputDStream;
import org.apache.spark.streaming.kafka010.ConsumerStrategies;
import org.apache.spark.streaming.kafka010.KafkaUtils;
import org.apache.spark.streaming.kafka010.LocationStrategies;
import org.apache.spark.util.Utils;
import org.locationtech.jts.index.SpatialIndex;
import scala.Tuple2;

import java.io.*;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.StreamSupport;

public class RihpsProcessorDynamic extends AbstractProcessor {

    public RihpsProcessorDynamic(String[] args) {
        super(args);
    }

    public static void main(String[] args) throws Exception {
        final AbstractProcessor processor = new RihpsProcessorDynamic(args);
        UserMetricsUtil.initialize(processor);
        DynamicDistributor dynamicRepartitioner = null;

        //check arguments
        String numberOfSubscriptions = processor.cmd.getOptionValue("number-of-subscriptions");
        final String indexType = processor.cmd.getOptionValue("type-of-index");
        final String spatialOperator = processor.cmd.getOptionValue("type-of-spatial-operator", "mixed").equals("mixed") ? ""
                : processor.cmd.getOptionValue("type-of-spatial-operator");
        final int numberOfPartitions = Integer.parseInt(processor.cmd.getOptionValue("number-of-partitions"));
        final boolean streamLocalityJoin = processor.cmd.getOptionValue(STREAM_LOCALITY_JOIN_OPTION_NAME, "false").equalsIgnoreCase("true");
        final boolean toDataBalance = processor.cmd.getOptionValue(TO_DATA_BALANCE_OPTION_NAME, "false").equalsIgnoreCase("true");
        if (processor.cmd.hasOption("type-of-grid")) {
            System.out.println("Too many parameters for this processor");
            System.exit(1);
        }

        //load and parse subscriptions
        String subscriptionsFile = "subscriptions-" + numberOfSubscriptions + spatialOperator + ".bin";
        List<Subscription> subscriptions = null;
        try ( ObjectInputStream ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream(subscriptionsFile)))) {
            subscriptions = (List<Subscription>) ois.readObject();
        }
        long sizeOfSubscriptions = RamUsageEstimator.sizeOf(subscriptions);
        processor.logger.info("Subscriptions size: " + RamUsageEstimator.humanReadableUnits(sizeOfSubscriptions));

        //add subscriptions to index
        SpatialIndex index = SpatialIndexFactory.createSmall(IndexType.valueOf(indexType), subscriptions);
        long sizeOfIndex = RamUsageEstimator.sizeOf(index);
        processor.logger.info("Index size: " + RamUsageEstimator.humanReadableUnits(sizeOfIndex));

        //partition subscriptions
        List<Tuple2<Integer, Map<Integer, Subscription>>> partitionedSubscriptions = PartitionedSubscriptionMapFactory.createPairs(subscriptions, numberOfPartitions);
        subscriptions = null; //free driver memory
        long sizeOfPartitionedSubscriptions = RamUsageEstimator.sizeOf(partitionedSubscriptions);
        processor.logger.info("Partitioned subscriptions size: " + RamUsageEstimator.humanReadableUnits(sizeOfPartitionedSubscriptions));

        RepartitionableRDDHandler<Integer, Map<Integer, Subscription>> repartitionableRDDHandler = null;

        if (!processor.cmd.hasOption("skip-processing")) {

            //broadcast index to all workers
            Broadcast<SpatialIndex> broadcastedIndex = processor.jsc.broadcast(index);
            index = null; //free driver memory

            final Partitioner keyPartitioner = new Partitioner() {
                @Override
                public int numPartitions() {
                    return numberOfPartitions;
                }

                @Override
                public int getPartition(Object o) {
                    return (Integer) o;
                }
            };

            final Partitioner publicationPartitioner = new Partitioner() {
                @Override
                public int numPartitions() {
                    return numberOfPartitions;
                }

                @Override
                public int getPartition(Object o) {
                    return Utils.nonNegativeMod(o.hashCode(), numberOfPartitions);
                }
            };

            final List<String> locations = HostsUtil.getExecutorHostsFromContext(processor.jsc.sc());
            System.out.println("Locations: " + locations);
            System.out.println("Executors num: " + processor.jsc.sc().defaultParallelism());
            repartitionableRDDHandler = new RepartitionableRDDHandler<>(processor.jsc, keyPartitioner, locations, partitionedSubscriptions);
            repartitionableRDDHandler.publishLocationsOfSubscriptions();
            AtomicReference<JavaPairRDD<Integer, Map<Integer, Subscription>>> repartitionableRDDReference = repartitionableRDDHandler.getRepartitionableRDDReference();

            partitionedSubscriptions = null; //free driver memory

            //subscribe to topic and get the stream
            final JavaInputDStream<ConsumerRecord<Void, Object>> stream = KafkaUtils.createDirectStream(processor.jssc, LocationStrategies.PreferConsistent(),
                    ConsumerStrategies.<Void, Object>Subscribe(processor.topics, processor.kafkaParams));

            final Function2<Set<Integer>, Set<Integer>, Set<Integer>> mergeCombiners = (set1, set2) -> {
                set1.addAll(set2);
                return set1;
            };

            //brodcast producer props to all workers
            Broadcast<Properties> broadcastedProperties = processor.jsc.broadcast(processor.producerProps);

            dynamicRepartitioner = DynamicDistributor.startDynamicRepartitionerThread(processor, numberOfPartitions, repartitionableRDDHandler,
                    locations, toDataBalance, processor.minTime, processor.mLDCBySecond);

            //process the stream
            stream.
                    map(record -> (Publication) record.value()).
                    mapToPair(publication -> {
                        List<Integer> candidateSubscriptionIds = broadcastedIndex.value().query(publication.getGeometry().getEnvelopeInternal());
                        return new Tuple2<>(publication, candidateSubscriptionIds);
                    }).
                    mapToPair(pair -> {
                        Map<Integer, Set<Integer>> partitionedCandidateSubscriptionIds = new HashMap<>();

                        for (int candidateSubscriptionId : pair._2) {
                            int partitionId = candidateSubscriptionId % numberOfPartitions;

                            partitionedCandidateSubscriptionIds.compute(partitionId, (key, oldSet) -> {
                                if (oldSet == null) {
                                    Set<Integer> newSet = new HashSet<>();
                                    newSet.add(candidateSubscriptionId);
                                    return newSet;
                                } else {
                                    oldSet.add(candidateSubscriptionId);
                                    return oldSet;
                                }
                            });
                        }

                        return new Tuple2<>(pair._1, partitionedCandidateSubscriptionIds);
                    }).
                    flatMapToPair(pair -> {

                        List<Tuple2<Integer, Tuple2<Publication, Set<Integer>>>> listOfPairs = new LinkedList<>();

                        for (Entry<Integer, Set<Integer>> entry : pair._2.entrySet()) {
                            listOfPairs.add(new Tuple2<>(entry.getKey(), new Tuple2<>(pair._1, entry.getValue())));
                        }

                        return listOfPairs.iterator();
                    }).
                    transformToPair(
                            rdd -> {
                                if (streamLocalityJoin) {
                                    rdd = rdd.partitionBy(keyPartitioner);
                                }

                                return rdd.
//                                    partitionBy(keyPartitioner).
                                        join(repartitionableRDDReference.get()).
                                        filter(UserMetricsUtil.<Tuple2<Tuple2<Publication, Set<Integer>>, Map<Integer, Subscription>>>publishPubsIpAndCountFunction()).
                                        values().
                                        mapToPair(pair -> {
                                            Set<Subscription> candidateSubscriptions = new HashSet<>();
                                            for (int subscriptionId : pair._1._2) {
                                                candidateSubscriptions.add(pair._2.get(subscriptionId));
                                            }
                                            return new Tuple2<>(pair._1._1, candidateSubscriptions);
                                        });
                            }
                    ).
                    //lower pipeline
                    mapToPair(pair -> {
                        long startTime = System.currentTimeMillis();
                        Set<Integer> satisfiedSubscriptions = new HashSet<>();

                        for (Subscription candidateSubscription : pair._2) {
                            if (candidateSubscription.getSpatialRelation().apply(candidateSubscription.getGeometry(), pair._1.getGeometry())) {
                                satisfiedSubscriptions.add(candidateSubscription.getId());
                            }
                        }
                        long endTime = System.currentTimeMillis();
                        UserMetricsUtil.publishPubsDurationMs(TaskContext.getPartitionId(), endTime-startTime);
                        return new Tuple2<>(pair._1, satisfiedSubscriptions);
                    }).
                    filter(pair -> pair._2.size() > 0).
                    reduceByKey(mergeCombiners, publicationPartitioner).
                    foreachRDD(rdd -> {
                        rdd.foreachPartition(iterator -> {
                            Producer<Publication, Set<Integer>> producer = new KafkaProducer<>(broadcastedProperties.value());

                            StreamSupport.
                                    stream(Spliterators.spliteratorUnknownSize(iterator, Spliterator.NONNULL), false).
                                    map(pair -> new ProducerRecord<>("geofil-matchings", pair._1, pair._2)).
                                    forEach(record -> producer.send(record));

//                            while (iterator.hasNext()) {
//                                Tuple2<Publication, Set<Integer>> pair = iterator.next();
//                                ProducerRecord<Publication, Set<Integer>> record = new ProducerRecord<>("geofil-matchings", pair._1, pair._2);
//                                producer.send(record);
//                            }
                            producer.close();
                        });
                    });

            //start our streaming context and wait for it to "finish"
            processor.jssc.start();
        }
        //add shutdown hook to collect statistics
        processor.addLoggingShutdownHook(numberOfPartitions,sizeOfSubscriptions, repartitionableRDDHandler.getRepartitionTimes());
        processor.waitUntilAllConsumed(processor, dynamicRepartitioner);
    }
}
