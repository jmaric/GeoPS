package hr.fer.retrofit.dynamic_join.geops_util.input_generator;

import hr.fer.retrofit.geofil.data.wrapping.AIdedPoint;
import org.locationtech.jts.index.SpatialIndex;

import java.util.Map;

public interface PointPartitionsLocator {
    Map<Integer, Integer> locatePartitions(AIdedPoint point);
}
