package hr.fer.retrofit.dynamic_join.geops_util.monitoring;

import hr.fer.retrofit.dynamic_join.distributor.DynamicDistributor;
import hr.fer.retrofit.geofil.distributed.spark.AbstractProcessor;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.groupon.metrics.UserMetricsSystem;
import scala.Tuple2;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class UserMetricsUtil {
    public static void initialize(AbstractProcessor processor){
        UserMetricsSystem.initialize(processor.jsc.sc(), DynamicDistributor.USER_METRICS_NAMESPACE);
    }

    public static void publishPrePubsIpAndIncCount(int partitionId, String ipAddress){
        UserMetricsSystem.counter(DynamicDistributor.PRE_COUNT_METRIC_PREFIX + partitionId).inc(1);
        UserMetricsSystem.gauge(DynamicDistributor.PRE_PUB_IP_METRIC_PREFIX + partitionId).set(ipAddress);
    }

    public static void publishPubsIpAndIncCount(int partitionId, String ipAddress){
        UserMetricsSystem.counter(DynamicDistributor.COUNT_METRIC_PREFIX + partitionId).inc(1);
        UserMetricsSystem.gauge(DynamicDistributor.PUB_IP_METRIC_PREFIX + partitionId).set(ipAddress);
    }

    public static void publishPubsDurationMs(int partitionId, long duration){
        UserMetricsSystem.counter(DynamicDistributor.COUNT_MS_METRIC_PREFIX + partitionId).inc((duration));
    }

    public static void publishSubIp(int partitionId, String ipAddress){
        UserMetricsSystem.gauge(DynamicDistributor.SUB_IP_METRIC_PREFIX + partitionId).set(ipAddress);
    }

    public static<T> Function<Tuple2<Integer, T>, Boolean> publishPubsIpAndCountFunction(){
        return tup -> {
            String ipAddress = null;
            try {
                ipAddress = InetAddress.getLocalHost().getHostAddress();
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }

            UserMetricsUtil.publishPubsIpAndIncCount(tup._1, ipAddress);
            return true;
        };
    }

    public static<T> Function<Tuple2<Integer, T>, Boolean> publishPrePubsIpAndCountFunction(){
        return tup -> {
            String ipAddress = null;
            try {
                ipAddress = InetAddress.getLocalHost().getHostAddress();
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }

            UserMetricsUtil.publishPrePubsIpAndIncCount(tup._1, ipAddress);
            return true;
        };
    }

}
