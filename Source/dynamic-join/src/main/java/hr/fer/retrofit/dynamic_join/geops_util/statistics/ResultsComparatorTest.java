package hr.fer.retrofit.dynamic_join.geops_util.statistics;

import scala.Tuple2;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

public class ResultsComparatorTest {
    public static void main(String[] args) throws ExecutionException, InterruptedException {

        ///////
        List<Tuple2<Integer, Set<Integer>>> res1Got = Arrays.asList(
                new Tuple2<Integer, Set<Integer>>(5, new HashSet<>(Arrays.asList(5,3,2,4,1))),
                new Tuple2<Integer, Set<Integer>>(2, new HashSet<>(Arrays.asList(2,4,1,7)))).stream().sorted((t1,t2) -> Integer.compare((int)t1._1, (int)(t2._1))).collect(Collectors.toList());
        List<Tuple2<Integer, Set<Integer>>> res2Got = Arrays.asList(
                new Tuple2<Integer, Set<Integer>>(2, new HashSet<>(Arrays.asList(2,1,7,4))),
                new Tuple2<Integer, Set<Integer>>(5, new HashSet<>(Arrays.asList(2,5,3,4,1)))).stream().sorted((t1,t2) -> Integer.compare((int)t1._1, (int)(t2._1))).collect(Collectors.toList());
        System.out.println("First:");
        System.out.println(res1Got);
        System.out.println("Second:");
        System.out.println(res2Got);
        System.out.println("Results are equal: " + res1Got.equals(res2Got));

        res1Got = Arrays.asList(
                new Tuple2<Integer, Set<Integer>>(350, new HashSet<>(Arrays.asList(1665, 5249, 5763, 388, 6789, 1414, 5767, 5641, 8587, 9358, 2318, 785, 401, 9876, 3092, 8216, 6049, 6820, 6692, 6309, 9129, 5419, 6188, 1709, 9134, 304, 6192, 6448, 7218, 3635, 437, 3642, 1084, 5820, 5826, 5699, 3523, 4037, 4422, 198, 1223, 6985, 6091, 6610, 6355, 84, 3156, 1750, 9431, 3292, 8924, 349, 1117, 7133, 3552, 9953, 7650, 8162, 9570, 867, 998, 2151, 3433, 6508, 9070, 7024, 6642, 114, 3572, 6262, 3190, 5625, 3577, 6780, 9727))),
                new Tuple2<Integer, Set<Integer>>(644, new HashSet<>(Arrays.asList(1665, 5249, 5763, 388, 6789, 1414, 5767, 5641, 8587, 9358, 2318, 785, 401, 9876, 3092, 8216, 6049, 6820, 6692, 6309, 9129, 5419, 6188, 1709, 9134, 304, 6192, 6448, 7218, 3635, 437, 3642, 1084, 5820, 5826, 5699, 3523, 4037, 4422, 198, 1223, 6985, 6091, 6610, 6355, 84, 3156, 1750, 9431, 8924, 3292, 349, 1117, 7133, 3552, 9953, 7650, 8162, 9570, 867, 998, 2151, 3433, 6508, 9070, 7024, 6642, 114, 3572, 6262, 3190, 5625, 3577, 6780, 9727)))).stream().map(t -> new Tuple2<Integer, Set<Integer>>(t._1, t._2)).sorted((t1,t2) -> Integer.compare((int)t1._1, (int)(t2._1))).collect(Collectors.toList());
        res2Got = Arrays.asList(
                new Tuple2<Integer, Set<Integer>>(350, new HashSet<>(Arrays.asList(1665, 5249, 5763, 388, 6789, 1414, 5767, 5641, 8587, 9358, 2318, 785, 401, 3092, 9876, 8216, 6049, 6820, 6692, 6309, 9129, 5419, 6188, 1709, 9134, 304, 6448, 6192, 7218, 3635, 437, 3642, 1084, 5820, 5826, 5699, 3523, 4037, 4422, 198, 1223, 6985, 6091, 6610, 6355, 84, 3156, 1750, 9431, 3292, 8924, 349, 7133, 1117, 3552, 9953, 7650, 8162, 9570, 867, 998, 2151, 3433, 6508, 9070, 7024, 6642, 114, 3572, 3190, 6262, 5625, 3577, 6780, 9727))),
                new Tuple2<Integer, Set<Integer>>(644, new HashSet<>(Arrays.asList(1665, 5249, 5763, 388, 6789, 1414, 5767, 5641, 8587, 9358, 2318, 785, 401, 9876, 3092, 8216, 6049, 6692, 6820, 6309, 9129, 5419, 6188, 1709, 9134, 304, 6192, 6448, 7218, 3635, 437, 3642, 1084, 5820, 5826, 5699, 3523, 4037, 4422, 198, 1223, 6985, 6091, 6610, 6355, 84, 3156, 1750, 9431, 8924, 3292, 349, 7133, 1117, 3552, 9953, 7650, 8162, 9570, 867, 998, 2151, 3433, 6508, 9070, 7024, 114, 6642, 3572, 3190, 6262, 5625, 3577, 6780, 9727)))).stream().map(t -> new Tuple2<Integer, Set<Integer>>(t._1, t._2)).sorted((t1,t2) -> Integer.compare((int)t1._1, (int)(t2._1))).collect(Collectors.toList());
        System.out.println("First:");
        System.out.println(res1Got);
        System.out.println("Second:");
        System.out.println(res2Got);
        System.out.println("Results are equal: " + res1Got.equals(res2Got));

    }
}
