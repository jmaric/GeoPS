package hr.fer.retrofit.dynamic_join.distributor;

import com.carrotsearch.sizeof.RamUsageEstimator;
import hr.fer.retrofit.dynamic_join.geops_util.monitoring.UserMetricsUtil;
import hr.fer.retrofit.geofil.distributed.spark.util2.NodeAffinityRDD;
import org.apache.spark.Partitioner;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.VoidFunction;
import org.apache.spark.rdd.RDD;
import scala.Tuple2;

import java.io.Serializable;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class RepartitionableRDDHandler<K, V> implements Serializable {

    private Partitioner partitioner;
    private List<String> locations;
    private int numberOfPartitions;
    private AtomicReference<JavaPairRDD<K, V>> repartitionableRDDReference;
    private AtomicReference<Map<Integer, String>> activeRepartitionStrategyReference;
    private List<Long> repartitionTimes;

    public RepartitionableRDDHandler(JavaSparkContext jsc, Partitioner partitioner, List<String> locations, List<Tuple2<K, V>> listToParallelize){
        this.partitioner = partitioner;
        this.locations = locations;
        this.repartitionableRDDReference = new AtomicReference<>();
        this.activeRepartitionStrategyReference = new AtomicReference<>(null);
        this.setRepartitionableRDD(listToParallelize, jsc);
        this.numberOfPartitions = numberOfPartitions;
        this.repartitionTimes = new ArrayList<>();
    }

    public RepartitionableRDDHandler(JavaSparkContext jsc, Partitioner partitioner, List<String> locations, List<Tuple2<K, V>> listToParallelize,
                    Map<Integer, String> partitionMapping){
        this.partitioner = partitioner;
        this.locations = locations;
        this.repartitionableRDDReference = new AtomicReference<>();
        this.activeRepartitionStrategyReference = new AtomicReference<>(null);
        this.setRepartitionableRDD(listToParallelize, partitionMapping, jsc);
        this.repartitionTimes = new ArrayList<>();

    }

    public AtomicReference<JavaPairRDD<K, V>> getRepartitionableRDDReference() {
        return repartitionableRDDReference;
    }
    
    public void setRepartitionableRDD(List<Tuple2<K, V>> listToParallelize, JavaSparkContext jsc) {
        System.out.println("Started here");
        RDD<Tuple2<K, V>> parallelized = jsc.parallelize(listToParallelize).rdd();
        long size = RamUsageEstimator.sizeOf(parallelized);
        System.out.println("SizeX: " + RamUsageEstimator.humanReadableUnits(size));
        JavaPairRDD<K, V> subscriptionRDD = (JavaPairRDD<K, V>)JavaPairRDD.fromJavaRDD(new NodeAffinityRDD<Tuple2<K, V>>(parallelized, locations, null, parallelized.elementClassTag()).toJavaRDD()).partitionBy(partitioner).cache();//PartitionMapper.transformStringToPartitioningMap(PartitionMapper.EXAMPLE_PARTITIONING)
        long size2 = RamUsageEstimator.sizeOf(parallelized);
        System.out.println("SizeY: " + RamUsageEstimator.humanReadableUnits(size2));
        this.repartitionableRDDReference.set(subscriptionRDD);
        System.out.println("Partitions: " + subscriptionRDD.partitions().stream().map(part -> part.index()).collect(Collectors.toList()));
    }

    public void setRepartitionableRDD(List<Tuple2<K, V>> listToParallelize, Map<Integer, String> partitionMapping, JavaSparkContext jsc) {
        RDD<Tuple2<K, V>> parallelized = jsc.parallelize(listToParallelize).rdd();
        JavaPairRDD<K, V> subscriptionRDD = (JavaPairRDD<K, V>)JavaPairRDD.fromJavaRDD(new NodeAffinityRDD<Tuple2<K, V>>(parallelized, locations, partitionMapping, parallelized.elementClassTag()).toJavaRDD()).partitionBy(partitioner).cache();//PartitionMapper.transformStringToPartitioningMap(PartitionMapper.EXAMPLE_PARTITIONING)
        this.repartitionableRDDReference.set(subscriptionRDD);
        System.out.println("Partitions: " + subscriptionRDD.partitions().stream().map(part -> part.index()).collect(Collectors.toList()));
        this.activeRepartitionStrategyReference.set(partitionMapping);
    }

    private void repartitionRDD(Map<Integer, String> partitionMapping) {
        this.repartitionableRDDReference.set((JavaPairRDD<K, V>)JavaPairRDD.fromJavaRDD(
                        new NodeAffinityRDD<Tuple2<K, V>>(repartitionableRDDReference.get().rdd(), locations,
                                partitionMapping, repartitionableRDDReference.get().rdd().elementClassTag()).toJavaRDD()).
                partitionBy(partitioner).cache());
        this.activeRepartitionStrategyReference.set(partitionMapping);
    }

    public void repartitionRDDandUpdateSubsLocations(Map<Integer, String> partitionMapping){
        long startTime = System.currentTimeMillis();
        repartitionRDD(partitionMapping);
        publishLocationsOfSubscriptions();
        this.repartitionTimes.add(System.currentTimeMillis()-startTime);
    }

    public List<Long> getRepartitionTimes(){
        return this.repartitionTimes;
    }

    public Map<Integer, String> getActiveRepartitionStrategy() {
        return activeRepartitionStrategyReference.get();
    }

    public void publishLocationsOfSubscriptions(){
        repartitionableRDDReference.get().foreach(new VoidFunction<Tuple2<K, V>>() {
            @Override
            public void call(Tuple2<K, V> value) throws Exception {
                String ipAddress = null;
                try {
                    ipAddress = InetAddress.getLocalHost().getHostAddress();
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                }
                UserMetricsUtil.publishSubIp(Integer.valueOf(value._1.toString()),ipAddress);
            }
        });
    }
}
