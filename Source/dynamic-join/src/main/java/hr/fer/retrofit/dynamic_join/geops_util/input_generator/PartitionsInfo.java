package hr.fer.retrofit.dynamic_join.geops_util.input_generator;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

public class PartitionsInfo {
    private Map<Integer, Integer> partitionsJobCountMap;
    private double pointSkewPercentage;

    public PartitionsInfo(Map<Integer, Integer> partitionsJobCountMap, List<Integer> skewPartitions) {
        this.partitionsJobCountMap = partitionsJobCountMap;
        this.pointSkewPercentage = calculatePointSkewPercentage(partitionsJobCountMap, skewPartitions);
    }

    public Map<Integer, Integer> getPartitionsJobCountMap() {
        return partitionsJobCountMap;
    }

    public void setPartitionsJobCountMap(Map<Integer, Integer> partitionsJobCountMap) {
        this.partitionsJobCountMap = partitionsJobCountMap;
    }

    public double getPointSkewPercentage() {
        return pointSkewPercentage;
    }

    public void setPointSkewPercentage(double pointSkewPercentage) {
        this.pointSkewPercentage = pointSkewPercentage;
    }

    public static double calculatePointSkewPercentage(Map<Integer, Integer> partitionsMapForPoint, List<Integer> skewPartitions){
        AtomicInteger skewPointJobsForPoint = new AtomicInteger();
        AtomicInteger totalPointJobsForPoint = new AtomicInteger();
        partitionsMapForPoint.entrySet().stream().forEach(entry -> {
            int partition = entry.getKey();
            int jobsCount = entry.getValue();

            if(skewPartitions.contains(partition)){
                skewPointJobsForPoint.addAndGet(jobsCount);
            }
            totalPointJobsForPoint.addAndGet(jobsCount);
        });

        return skewPointJobsForPoint.get()*1.0/totalPointJobsForPoint.get();
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PartitionsInfo that = (PartitionsInfo) o;
        return Double.compare(that.pointSkewPercentage, pointSkewPercentage) == 0 && Objects.equals(partitionsJobCountMap, that.partitionsJobCountMap);
    }

    @Override
    public int hashCode() {
        return Objects.hash(partitionsJobCountMap, pointSkewPercentage);
    }

    @Override
    public String toString() {
        return "PartitionsInfo{" +
                "partitionsJobCountMap=" + partitionsJobCountMap +
                ", pointSkewPercentage=" + pointSkewPercentage +
                '}';
    }
}
