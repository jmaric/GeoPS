package hr.fer.retrofit.dynamic_join.geops_util.input_generator;

import hr.fer.retrofit.geofil.data.wrapping.AIdedPoint;
import org.locationtech.jts.index.SpatialIndex;

import java.util.AbstractMap;
import java.util.Map;
import java.util.stream.Collectors;

public class RispsPointPartitionsLocator implements PointPartitionsLocator{

    private SpatialIndex index;

    public RispsPointPartitionsLocator(SpatialIndex index){
        this.index = index;
    }

    @Override
    public Map<Integer, Integer> locatePartitions(AIdedPoint point) {
        Map<Integer, Integer> partitions = (Map<Integer, Integer>) index.query(point.getPoint().getEnvelopeInternal()).stream().map((Object se) -> ((AbstractMap.SimpleEntry)se).getKey()).collect(Collectors.groupingBy(x -> x, Collectors.summingInt(x -> 1)));
        return partitions;
    }
}
