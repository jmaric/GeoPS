package hr.fer.retrofit.dynamic_join.distributor;

import com.google.common.base.Splitter;
import hr.fer.retrofit.dynamic_join.geops_util.statistics.SimpleHost;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.Collections.reverseOrder;

public class PartitionMapper {
    private Map<Integer, Partition> idToPartitionMap;
    private List<String> executors;
    public static final String EXAMPLE_PARTITIONING = "0=worker16.streamslab.fer.hr, 1=worker07.streamslab.fer.hr, 2=worker02.streamslab.fer.hr, 3=worker13.streamslab.fer.hr, 4=worker08.streamslab.fer.hr, 5=worker05.streamslab.fer.hr, 6=worker03.streamslab.fer.hr, 7=worker04.streamslab.fer.hr, 8=worker06.streamslab.fer.hr, 9=worker09.streamslab.fer.hr, 10=worker10.streamslab.fer.hr, 11=worker03.streamslab.fer.hr, 12=worker05.streamslab.fer.hr, 13=worker05.streamslab.fer.hr, 14=worker04.streamslab.fer.hr, 15=worker10.streamslab.fer.hr, 16=worker01.streamslab.fer.hr, 17=worker04.streamslab.fer.hr, 18=worker09.streamslab.fer.hr, 19=worker15.streamslab.fer.hr, 20=worker05.streamslab.fer.hr, 21=worker14.streamslab.fer.hr, 22=worker10.streamslab.fer.hr, 23=worker13.streamslab.fer.hr, 24=worker02.streamslab.fer.hr, 25=worker11.streamslab.fer.hr, 26=worker10.streamslab.fer.hr, 27=worker08.streamslab.fer.hr, 28=worker15.streamslab.fer.hr, 29=worker16.streamslab.fer.hr, 30=worker04.streamslab.fer.hr, 31=worker08.streamslab.fer.hr, 32=worker09.streamslab.fer.hr, 33=worker07.streamslab.fer.hr, 34=worker15.streamslab.fer.hr, 35=worker14.streamslab.fer.hr, 36=worker02.streamslab.fer.hr, 37=worker01.streamslab.fer.hr, 38=worker06.streamslab.fer.hr, 39=worker04.streamslab.fer.hr, 40=worker16.streamslab.fer.hr, 41=worker04.streamslab.fer.hr, 42=worker03.streamslab.fer.hr, 43=worker11.streamslab.fer.hr, 44=worker13.streamslab.fer.hr, 45=worker02.streamslab.fer.hr, 46=worker09.streamslab.fer.hr, 47=worker03.streamslab.fer.hr, 48=worker08.streamslab.fer.hr, 49=worker07.streamslab.fer.hr, 50=worker11.streamslab.fer.hr, 51=worker09.streamslab.fer.hr, 52=worker15.streamslab.fer.hr, 53=worker15.streamslab.fer.hr, 54=worker07.streamslab.fer.hr, 55=worker05.streamslab.fer.hr, 56=worker11.streamslab.fer.hr, 57=worker09.streamslab.fer.hr, 58=worker06.streamslab.fer.hr, 59=worker02.streamslab.fer.hr, 60=worker14.streamslab.fer.hr, 61=worker07.streamslab.fer.hr, 62=worker06.streamslab.fer.hr, 63=worker11.streamslab.fer.hr, 64=worker10.streamslab.fer.hr, 65=worker10.streamslab.fer.hr, 66=worker05.streamslab.fer.hr, 67=worker09.streamslab.fer.hr, 68=worker15.streamslab.fer.hr, 69=worker13.streamslab.fer.hr, 70=worker11.streamslab.fer.hr, 72=worker15.streamslab.fer.hr, 73=worker07.streamslab.fer.hr, 74=worker16.streamslab.fer.hr, 75=worker10.streamslab.fer.hr, 76=worker04.streamslab.fer.hr, 77=worker13.streamslab.fer.hr, 78=worker14.streamslab.fer.hr, 79=worker08.streamslab.fer.hr, 80=worker06.streamslab.fer.hr, 81=worker08.streamslab.fer.hr, 82=worker16.streamslab.fer.hr, 83=worker08.streamslab.fer.hr, 84=worker08.streamslab.fer.hr, 85=worker05.streamslab.fer.hr, 86=worker13.streamslab.fer.hr, 87=worker10.streamslab.fer.hr, 88=worker07.streamslab.fer.hr, 89=worker02.streamslab.fer.hr, 90=worker16.streamslab.fer.hr, 93=worker07.streamslab.fer.hr, 94=worker05.streamslab.fer.hr, 95=worker09.streamslab.fer.hr, 96=worker14.streamslab.fer.hr, 97=worker03.streamslab.fer.hr, 98=worker06.streamslab.fer.hr, 99=worker01.streamslab.fer.hr, 100=worker01.streamslab.fer.hr, 101=worker11.streamslab.fer.hr, 102=worker01.streamslab.fer.hr, 103=worker13.streamslab.fer.hr, 104=worker15.streamslab.fer.hr, 105=worker16.streamslab.fer.hr, 106=worker14.streamslab.fer.hr, 107=worker14.streamslab.fer.hr, 108=worker03.streamslab.fer.hr, 109=worker03.streamslab.fer.hr, 110=worker01.streamslab.fer.hr, 111=worker01.streamslab.fer.hr, 112=worker11.streamslab.fer.hr, 113=worker06.streamslab.fer.hr, 114=worker16.streamslab.fer.hr, 115=worker02.streamslab.fer.hr, 116=worker02.streamslab.fer.hr, 117=worker13.streamslab.fer.hr, 120=worker14.streamslab.fer.hr";


    public PartitionMapper() {
        idToPartitionMap = new TreeMap<>();
    }

    public PartitionMapper(List<String> executors) {
        this();
        this.executors = executors;
    }

    public void putPartition(int id, Partition partition){
        idToPartitionMap.put(id, partition);
    }

    public void putPartition(int id, String host, long count){
        idToPartitionMap.put(id, Partition.of(id, host, count));
    }

    public static class RepartitionSuggestion {
        private List<Long> locationCounts;
        private Map<Integer, String> repartitionMap;

        public RepartitionSuggestion(List<Long> countPerLocation, Map<Integer, String> repartitionMap) {
            this.locationCounts = countPerLocation;
            this.repartitionMap = repartitionMap;
        }

        public List<Long> getLocationCounts() {
            return locationCounts;
        }

        public void setLocationCounts(List<Long> locationCounts) {
            this.locationCounts = locationCounts;
        }

        public Map<Integer, String> getRepartitionMap() {
            return repartitionMap;
        }

        public void setRepartitionMap(Map<Integer, String> repartitionMap) {
            this.repartitionMap = repartitionMap;
        }
    }

    public RepartitionSuggestion repartitionGreedy(int hostsCount){
        List<Partition> partitionsCopy = new ArrayList<>(idToPartitionMap.values()).stream().sorted(Comparator.comparingLong(Partition::getLoad).reversed()).collect(Collectors.toList());
        Queue<Partition> partitionQueue = new ArrayDeque<>(partitionsCopy);

        PriorityQueue<SimpleHost> hostPriorityQueue =
                new PriorityQueue<>(hostsCount, Comparator.comparingLong(SimpleHost::getSum));
        for (int i = 0; i < hostsCount; i++) {
            hostPriorityQueue.add(new SimpleHost());
        }

        while (!partitionQueue.isEmpty()) {
            Partition partition = partitionQueue.poll();
            SimpleHost lowestSumHost = hostPriorityQueue.poll();
            lowestSumHost.increaseSum(partition.getId(), partition.getLoad());
            hostPriorityQueue.add(lowestSumHost);
        }

        Map<Integer, String> partitionIdsToHostsMap = new HashMap<>();
        System.out.println("Host priority queue: " + new ArrayList<>(hostPriorityQueue));

        int i = 0;
        for(SimpleHost sh : hostPriorityQueue){
            String host = executors.get(i);
            sh.getPartitionIds().forEach(pid -> partitionIdsToHostsMap.put(pid, host));
            i++;
        }
        return new RepartitionSuggestion(hostPriorityQueue.stream().map(sh -> sh.getSum()).collect(Collectors.toList()), partitionIdsToHostsMap);
    }

    public Map<String, Long> calculateCountPerHost(){
        return idToPartitionMap.values().stream().collect(
                Collectors.toMap(Partition::getLocation, Partition::getLoad, (Long c1, Long c2) -> Long.valueOf(c1+c2)));
    }

    public static Double calculateDifferenceCoefFromCountsList(List<Long> locationCounts){
        List<Long> sortedList = locationCounts.stream().sorted().collect(Collectors.toList());
        System.out.println("Counts per host: " + sortedList);
        OptionalDouble average = sortedList
                .stream()
                .mapToDouble(a -> a)
                .average();
        if(average.isPresent()){
            double averageUnwrapped = average.getAsDouble();
            double maxDiff = sortedList.get(sortedList.size()-1)-averageUnwrapped;
            return maxDiff/averageUnwrapped;
        }
        return null;
    }

    //returns maxDifference between a count and an average count divided by average
    public Double calculateDifferenceCoef(){
        return calculateDifferenceCoefFromCountsList(new ArrayList<>(calculateCountPerHost().values()));
    }

    public void setCount(int id, long count){
        if(idToPartitionMap.containsKey(id)){
            Partition partition = idToPartitionMap.get(id);
            partition.setCount(count);
            partition.getLastCountsQueue().add(count);
        }else{
            idToPartitionMap.put(id, Partition.of(id, null, count));
        }
    }

    public void setLocation(int id, String host){
        if(idToPartitionMap.containsKey(id)){
            idToPartitionMap.get(id).setLocation(host);
        }else{
            idToPartitionMap.put(id, Partition.of(id, host, 0));
        }
    }

    public Partition getPartition(int id){
        return idToPartitionMap.get(id);
    }

    public static Map<Integer, String> transformStringToPartitioningMap(String partitionSuggestion){
        return Splitter.on(",")
                .withKeyValueSeparator("=")
                .split(partitionSuggestion)
                .entrySet().stream().map(e -> new AbstractMap.SimpleEntry<>(Integer.valueOf(e.getKey().trim()), e.getValue().trim())).collect(Collectors.toMap(
                        entry
                                -> entry.getKey(),
                        entry -> entry.getValue()));
    }

    @Override
    public String toString() {
        return "PartitionMapper{" +
                "idToPartitionMap=" + idToPartitionMap +
                '}';
    }
}
