package hr.fer.retrofit.geofil.distributed.spark;

import com.carrotsearch.sizeof.RamUsageEstimator;
import hr.fer.retrofit.geofil.data.indexing.PartitionedSubscriptionsFactory;
import hr.fer.retrofit.geofil.data.model.Publication;
import hr.fer.retrofit.geofil.data.model.Subscription;
import hr.fer.retrofit.geofil.data.partitioning.SpatialPartitionerFactory;
import hr.fer.retrofit.dynamic_join.geops_util.monitoring.UserMetricsUtil;
import hr.fer.retrofit.dynamic_join.distributor.RepartitionableRDDHandler;
import hr.fer.retrofit.geofil.distributed.spark.util.HostsUtil;
import hr.fer.retrofit.dynamic_join.distributor.DynamicDistributor;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.spark.Partitioner;
import org.apache.spark.TaskContext;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.broadcast.Broadcast;
import org.apache.spark.streaming.api.java.JavaInputDStream;
import org.apache.spark.streaming.kafka010.ConsumerStrategies;
import org.apache.spark.streaming.kafka010.KafkaUtils;
import org.apache.spark.streaming.kafka010.LocationStrategies;
import org.apache.spark.util.Utils;
import org.datasyslab.geospark.enums.GridType;
import org.datasyslab.geospark.spatialPartitioning.SpatialPartitioner;
import org.locationtech.jts.geom.Geometry;
import scala.Tuple2;

import java.io.*;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.StreamSupport;

public class SpsProcessorDynamic extends AbstractProcessor {

    public SpsProcessorDynamic(String[] args) {
        super(args);
    }

    public static void main(String[] args) throws Exception {
        final AbstractProcessor processor = new SpsProcessorDynamic(args);
        UserMetricsUtil.initialize(processor);
        DynamicDistributor dynamicRepartitioner = null;

        //check arguments
        String numberOfSubscriptions = processor.cmd.getOptionValue("number-of-subscriptions");
        final String spatialOperator = processor.cmd.getOptionValue("type-of-spatial-operator", "mixed").equals("mixed") ? ""
                : processor.cmd.getOptionValue("type-of-spatial-operator");
        final int numberOfPartitions = Integer.parseInt(processor.cmd.getOptionValue("number-of-partitions"));
        final String gridType = processor.cmd.getOptionValue("type-of-grid");
        final boolean streamLocalityJoin = processor.cmd.getOptionValue(STREAM_LOCALITY_JOIN_OPTION_NAME, "false").equalsIgnoreCase("true");
        final boolean toDataBalance = processor.cmd.getOptionValue(TO_DATA_BALANCE_OPTION_NAME, "false").equalsIgnoreCase("true");

        if (processor.cmd.hasOption("type-of-index")) {
            System.out.println("Too many parameters for this processor");
            System.exit(1);
        }

        //load and parse subscriptions
        //String pathPrefix = "/home/josip/geofil/geofil/";
        String subscriptionPathPrefix = processor.cmd.getOptionValue("sup", "");
        String subscriptionsFile = subscriptionPathPrefix + "subscriptions-" + numberOfSubscriptions + spatialOperator + ".bin";
        List<Subscription> subscriptions = null;
        try ( ObjectInputStream ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream(subscriptionsFile)))) {
            subscriptions = (List<Subscription>) ois.readObject();
        }
        System.out.println("Subscriptions created");
        long sizeOfSubscriptions = RamUsageEstimator.sizeOf(subscriptions);
        processor.logger.info("Subscriptions size: " + RamUsageEstimator.humanReadableUnits(sizeOfSubscriptions));

        //create a partitioner using subscriptions envelopes
        final SpatialPartitioner partitioner = SpatialPartitionerFactory.createExactNumberOfPartitions(GridType.valueOf(gridType), numberOfPartitions, subscriptions);
        processor.logger.info("Number of partitioner partitions: " + partitioner.numPartitions());
        long sizeOfPartitioner = RamUsageEstimator.sizeOf(partitioner);
        processor.logger.info("Partitioner size: " + RamUsageEstimator.humanReadableUnits(sizeOfPartitioner));
        final int exactNumberOfPartitions = partitioner.numPartitions();

        List<Tuple2<Integer, Set<Subscription>>> partitionedSubscriptions = PartitionedSubscriptionsFactory.createPairs(subscriptions, partitioner);
        System.out.println("Partitioned subscriptions created");
        long sizeOfPartitionedSubscriptions = RamUsageEstimator.sizeOf(partitionedSubscriptions);
        processor.logger.info("Partitioned subscriptions size: " + RamUsageEstimator.humanReadableUnits(sizeOfPartitionedSubscriptions));

        subscriptions = null; //free driver memory

        RepartitionableRDDHandler<Integer, Set<Subscription>> repartitionableRDDHandler = null;

        if (!processor.cmd.hasOption("skip-processing")) {

            //broadcast partitioner to all workers        
            final Broadcast<SpatialPartitioner> broadcastedPartitioner = processor.jsc.broadcast(partitioner);

            //make PairRDD partitioner which partitions its according to key = partition id
            final Partitioner keyPartitioner = new Partitioner() {

                @Override
                public int numPartitions() {
                    return exactNumberOfPartitions;
                }

                @Override
                public int getPartition(Object o) {
                    return (Integer) o;
                }
            };

            final Partitioner publicationPartitioner = new Partitioner() {
                @Override
                public int numPartitions() {
                    return exactNumberOfPartitions;
                }

                @Override
                public int getPartition(Object o) {
                    return Utils.nonNegativeMod(o.hashCode(), exactNumberOfPartitions);
                }
            };

//            List<String> locations = HostsUtil.getExecutorLocationsFromContext(processor.jsc.sc());

            final List<String> locations = HostsUtil.getExecutorHostsFromContext(processor.jsc.sc());
            System.out.println("Locations: " + locations);
            System.out.println("Executors num: " + processor.jsc.sc().defaultParallelism());
            System.out.println("Here1");
            repartitionableRDDHandler = new RepartitionableRDDHandler<>(processor.jsc, keyPartitioner, locations, partitionedSubscriptions);
            System.out.println("Here2");
            System.out.println("Here3");
            System.out.println("Passed");
            repartitionableRDDHandler.publishLocationsOfSubscriptions();
            System.out.println("Here4");
            AtomicReference<JavaPairRDD<Integer, Set<Subscription>>> repartitionableRDDReference = repartitionableRDDHandler.getRepartitionableRDDReference();
            System.out.println("Here5");

            partitionedSubscriptions = null; //free driver memory

            //subscribe to topic and get the stream
            final JavaInputDStream<ConsumerRecord<Void, Object>> stream = KafkaUtils.createDirectStream(processor.jssc, LocationStrategies.PreferConsistent(),
                    ConsumerStrategies.<Void, Object>Subscribe(processor.topics, processor.kafkaParams));

            //brodcast producer props
            final Broadcast<Properties> broadcastedProperties = processor.jsc.broadcast(processor.producerProps);

            final Function2<Set<Integer>, Set<Integer>, Set<Integer>> mergeCombiners = (Set<Integer> set1, Set<Integer> set2) -> {
                set1.addAll(set2);
                return set1;
            };


            dynamicRepartitioner = DynamicDistributor.startDynamicRepartitionerThread(processor, exactNumberOfPartitions, repartitionableRDDHandler,
                    locations, toDataBalance, processor.minTime, processor.mLDCBySecond);

            //process the stream
            stream.
                    map(record -> (Publication) record.value()).
                    mapToPair(publication -> {
                        Iterator<Tuple2<Integer, Geometry>> iterator = broadcastedPartitioner.getValue().placeObject((publication).getGeometry());//vraća potencijalne particije
                        return new Tuple2<>(publication, iterator);
                    }).
                    flatMapToPair(pair -> {//vraća se stream parova (publikacija, potencijalna particija za pretplatu)
                                List<Tuple2<Integer, Publication>> partitionIdPublicationList = new LinkedList<>();
                                pair._2.forEachRemaining(partitionIdGeometryPair -> {
                                    partitionIdPublicationList.add(new Tuple2<>(partitionIdGeometryPair._1, pair._1));
                                });
                                return partitionIdPublicationList.iterator();
                            }
                    ).
                    transformToPair(
                            (JavaPairRDD<Integer, Publication> rdd) ->{
                                if(streamLocalityJoin){
                                    rdd = rdd.partitionBy(keyPartitioner);
                                }
                                return rdd.
//                                        filter(UserMetricsUtil.<Publication>publishPrePubsIpAndCountFunction()).
                                        join(repartitionableRDDReference.get()).
                                        filter(UserMetricsUtil.<Tuple2<Publication, Set<Subscription>>>publishPubsIpAndCountFunction()).
                                        mapToPair(pair -> pair._2);
                            }

                            ).
                    //lower pipeline
                    mapToPair(new PairFunction<Tuple2<Publication, Set<Subscription>>, Publication, Set<Integer>>() {
                        @Override
                        public Tuple2<Publication, Set<Integer>> call(Tuple2<Publication, Set<Subscription>> pair) throws Exception {
                            long startTime = System.currentTimeMillis();
                            Set<Integer> satisfiedSubscriptions = new HashSet<>();

                            for (Subscription candidateSubscription : pair._2) {
                                if (candidateSubscription.getSpatialRelation().apply(candidateSubscription.getGeometry(), pair._1.getGeometry())) {
                                    satisfiedSubscriptions.add(candidateSubscription.getId());
                                }
                            }
                            long endTime = System.currentTimeMillis();
                            UserMetricsUtil.publishPubsDurationMs(TaskContext.getPartitionId(), endTime-startTime);
                            return new Tuple2<>(pair._1, satisfiedSubscriptions);
                        }

                    }).
                    filter(pair -> pair._2.size() > 0).
                    reduceByKey(mergeCombiners, publicationPartitioner).
                    foreachRDD(rdd -> {
                        rdd.foreachPartition(iterator -> {
                            Producer<Publication, Set<Integer>> producer = new KafkaProducer<>(broadcastedProperties.value());

                            StreamSupport.
                                    stream(Spliterators.spliteratorUnknownSize(iterator, Spliterator.NONNULL), false).
                                    map(pair -> new ProducerRecord<>("geofil-matchings", pair._1, pair._2)).
                                    forEach(record -> producer.send(record));

                            producer.close();
                        });
                    });

            //start our streaming context and wait for it to "finish"
            processor.jssc.start();
        }

        //add shutdown hook to collect statistics
        processor.addLoggingShutdownHook(exactNumberOfPartitions,sizeOfSubscriptions, repartitionableRDDHandler.getRepartitionTimes());
        processor.waitUntilAllConsumed(processor, dynamicRepartitioner);
    }

    public static org.apache.spark.api.java.Optional<Long> updateFunction(List<Long> values, org.apache.spark.api.java.Optional<Long> state){
        Long newState = (Long) state.orElse(0L) + values.stream().reduce(0L, Long::sum);
        return org.apache.spark.api.java.Optional.of(newState);
    }
}
