package hr.fer.retrofit.dynamic_join.geops_util.input_generator;

import hr.fer.retrofit.geofil.data.loading.AccidentDataLoader;
import hr.fer.retrofit.geofil.data.loading.PostcodeDataLoader;
import hr.fer.retrofit.geofil.data.model.Publication;
import hr.fer.retrofit.geofil.data.model.Subscription;
import hr.fer.retrofit.geofil.data.partitioning.SpatialPartitionerFactory;
import hr.fer.retrofit.geofil.data.wrapping.AIdedPoint;
import hr.fer.retrofit.geofil.data.wrapping.CodedGeometry;
import hr.fer.retrofit.geofil.distributed.kafka.serde.ObjectSerializer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.IntegerSerializer;
import org.datasyslab.geospark.enums.ProcessorType;
import org.datasyslab.geospark.spatialPartitioning.SpatialPartitioner;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.geom.PrecisionModel;
import org.locationtech.jts.index.SpatialIndex;
import org.locationtech.jts.index.strtree.STRtree;
import scala.Tuple2;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.LongAdder;
import java.util.stream.Collectors;

public class SpecificSkewedPublicationProducer2 {

    private static final double AREA_PERCENTAGE = 0.1;
    private static final double DISTRICT_PERCENTAGE = 0.2;
    private static final double SECTOR_PERCENTAGE = 0.3;
    private static final double POLYGON_PERCENTAGE = AREA_PERCENTAGE + DISTRICT_PERCENTAGE + SECTOR_PERCENTAGE;
    //POINT_PERCENTAGE is 1 - (AREA_PERCENTAGE + DISTRICT_PERCENTAGE + SECTOR_PERCENTAGE)

    public static void main(String[] args) throws Exception {
        //check arguments
        int numberOfPublications = 0;

        String topic = "";
        if (args.length != 7) {
            System.out.println("PublicationProducer topic_name number_of_publications point_percentage skew_percentage grid_type number_of_partitions subscriptions_file");
            System.exit(1);
        } else {
            final double pointPercentage, areaPercentage, districtPercentage, sectorPercentage,  skewPercentage;
            final String gridType;
            final int partitionsNum;
            numberOfPublications = Integer.parseInt(args[1]);
            topic = args[0];
            pointPercentage = Double.parseDouble(args[2]);
            skewPercentage = Double.parseDouble(args[3]);
            gridType = args[4];
            partitionsNum = Integer.parseInt(args[5]);
            String subscriptionsFile = args[6];
            List<Subscription> subscriptions = null;
            try ( ObjectInputStream ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream(subscriptionsFile)))) {
                subscriptions = (List<Subscription>) ois.readObject();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

            final SpatialPartitioner partitioner = SpatialPartitionerFactory.createExactNumberOfPartitions(SpecificSkewedPublicationProducer.getGridTypeFromProcessorType(ProcessorType.valueOf(gridType)), partitionsNum, subscriptions);
            SpatialIndex index = null;
//            if(GridType.valueOf(gridType) == (GridType.KDBTREE)) {
//                String indexType = "HPR_TREE";
//                index = SpatialIndexFactory.createSmallWithPartitions(SpatialIndexFactory.IndexType.valueOf(indexType), subscriptions, partitioner);
//            }
            areaPercentage = (1 - pointPercentage) * AREA_PERCENTAGE / POLYGON_PERCENTAGE;
            districtPercentage = (1 - pointPercentage) * DISTRICT_PERCENTAGE / POLYGON_PERCENTAGE;
            sectorPercentage = (1 - pointPercentage) * SECTOR_PERCENTAGE / POLYGON_PERCENTAGE;

            System.out.println("Area percentage: " + areaPercentage);

            System.out.println("Result number of partitions: " + partitioner.numPartitions());

            //load data
            System.out.println("Loading...");

            //presision of 10 decimal digits
            int decimals = 10;
            GeometryFactory gf = new GeometryFactory(new PrecisionModel(Math.pow(10, decimals)), 4326);

            //load areas
            String filePath = "GB_Postcodes-QGIS_fixed/PostalArea_fixed.shp";
            List<CodedGeometry> areas = PostcodeDataLoader.loadShapeFile(filePath, gf);
            System.out.println("Areas: " + areas.size());

            STRtree areaIndex = new STRtree();
            for (CodedGeometry area : areas) {
                areaIndex.insert(area.getGeometry().getEnvelopeInternal(), area);
            }
            areaIndex.build();

            //load districts
            filePath = "GB_Postcodes-QGIS_fixed/PostalDistrict_fixed.shp";
            List<CodedGeometry> districts = PostcodeDataLoader.loadShapeFile(filePath, gf);
            System.out.println("Districts: " + districts.size());

            STRtree districtIndex = new STRtree();
            for (CodedGeometry district : districts) {
                districtIndex.insert(district.getGeometry().getEnvelopeInternal(), district);
            }
            districtIndex.build();

            //load sectors
            filePath = "GB_Postcodes-QGIS_fixed/PostalSector_fixed.shp";
            List<CodedGeometry> sectors = PostcodeDataLoader.loadShapeFile(filePath, gf);
            System.out.println("Sectors: " + sectors.size());

            STRtree sectorIndex = new STRtree();
            for (CodedGeometry sector : sectors) {
                sectorIndex.insert(sector.getGeometry().getEnvelopeInternal(), sector);
            }
            sectorIndex.build();

            //load points
            filePath = "dft-accident-data/Accidents0515.csv";
            ArrayList<AIdedPoint> codedPoints = AccidentDataLoader.loadCsvFile(filePath, gf);
            System.out.println("Points: " + codedPoints.size());

            //load london points
            GeometryFactory geoFac = new GeometryFactory();
            List<String> londonCodes = Arrays.asList("NW", "N", "E", "W", "WC", "EC", "SW", "SE", "EN", "IG", "RM", "DA", "BR", "CR", "SM", "KT", "TW", "UB", "HA", "WD", "EN");
            Geometry londonGeometry = geoFac.buildGeometry(areas.stream().filter(cg -> londonCodes.contains(cg.getCode())).map(cg -> cg.getGeometry()).collect(Collectors.toList())).union();
            londonGeometry = londonGeometry.getEnvelope();
            ArrayList<AIdedPoint> codedPointsLondon = AccidentDataLoader.loadFilteredCsvFile(filePath, gf, londonGeometry);
            System.out.println("London points: " + codedPointsLondon.size());

            //generate publications
            System.out.println("Generating...");

            //parallelize
            int numberOfProcessors = Runtime.getRuntime().availableProcessors();
            ExecutorService executor = Executors.newFixedThreadPool(numberOfProcessors - 1);
            final int numberOfPublicationsPerTask = (int) (1.2 * Math.ceil(numberOfPublications / (numberOfProcessors - 1d)));

            AtomicInteger pubId = new AtomicInteger(0);
            System.out.println("Part num: " + partitionsNum);
            List<Integer> workerPartitions = fillUpWorkerPartitions(ProcessorType.valueOf(gridType), 3, partitionsNum);
            List<Integer> forbiddenPartitions = new ArrayList<>();
//            if(GridType.valueOf(gridType) == (GridType.KDBTREE)) {
//                forbiddenPartitions.add(197);
//                forbiddenPartitions.add(24);
//                forbiddenPartitions.add(38);
//                forbiddenPartitions.add(0);
//                forbiddenPartitions.add(2);
////                forbiddenPartitions.addAll(workerPartitions);
////                System.out.println("Added 15 to forbidden.");
//            }

            SpatialIndex finalIndex1 = index;

            AtomicInteger atomicI = new AtomicInteger();

//            List<Tuple2<Integer, Map<Integer, Integer>>> codedPointsPartitions = codedPoints.stream()
//                    .map(point -> new Tuple2<>(atomicI.getAndIncrement(), findPartitionsFromIndex(finalIndex1, point, forbiddenPartitions)))
////                                .filter(tuple -> tuple._2.size() > 0 )
////                    .filter(parts -> parts.size() == 1 || parts.size() == 2 || parts.size() == 3)
////                                .map(tuple -> tuple._2.keySet().stream().findFirst().get()).distinct()
////                                .sorted(Comparator.comparingInt(i -> i))
//                    .sorted(Comparator.comparingInt(t -> t._1))
//                    .collect(Collectors.toList());



            System.out.println(workerPartitions);
            SpatialIndex finalIndex = index;
            Callable<List<Publication>> generateTask = () -> {
                List<Publication> publicationsPerTask = new LinkedList<>();
                int skewPoints = 0;
                int totalPoints = 0;

                List<Tuple2<Integer, Long>> points = new ArrayList<>();

                while (publicationsPerTask.size() < numberOfPublicationsPerTask) {
                    //randomly generate a point

                    Integer randPartition = workerPartitions.get(ThreadLocalRandom.current().nextInt(workerPartitions.size()));
                    AIdedPoint point = null;
                    int pointId;
                    if(ProcessorType.valueOf(gridType) == (ProcessorType.RISPS)) {
                        int partition;
                        if ((totalPoints == 0 || (skewPoints / totalPoints) < skewPercentage)) {
                            int attempts = 0;
                            do {
                                pointId = ThreadLocalRandom.current().nextInt(codedPoints.size());
                                point = codedPoints.get(pointId);
                                if(attempts > 100000){
                                    while(workerPartitions.contains(randPartition))workerPartitions.remove(randPartition);
                                    randPartition = workerPartitions.get(ThreadLocalRandom.current().nextInt(workerPartitions.size()));
                                    System.out.println("Worker partitions size: " + workerPartitions.size() + ", attempts: " + attempts);
                                    attempts = 0;
                                }
                                attempts++;
                            } while (!randPartition.equals(findMaxPartitionFromIndex(finalIndex, point, forbiddenPartitions)));//partitioner.placeObject(point.getPoint()).next()._1));
                            totalPoints++;
                            skewPoints++;
                        } else {
                            do {
                                pointId = ThreadLocalRandom.current().nextInt(codedPoints.size());
                                point = codedPoints.get(pointId);
                                if(point == null || finalIndex == null || forbiddenPartitions == null){
                                    System.out.println("OUT:" + point + "|" + finalIndex + "|" + forbiddenPartitions);
                                }
                                partition = findMaxPartitionFromIndex(finalIndex, point, forbiddenPartitions);
                            } while (forbiddenPartitions.contains(partition));
                            totalPoints++;
                        }
//                        System.out.println("Current pointId: " + pointId + ", partition: " + partitioner.placeObject(point.getPoint()).next()._1 +  ", index: " + finalIndex.query(point.getPoint().getEnvelopeInternal()).stream().map((Object se) -> ((AbstractMap.SimpleEntry)se).getKey()).count());//finalIndex.query(point.getPoint().getEnvelopeInternal()).stream().map((Object se) -> ((AbstractMap.SimpleEntry)se).getKey()).collect(Collectors.groupingBy(x -> x, Collectors.summingInt(x -> 1))));
//                        Map<Integer, Integer> partitions = (Map<Integer, Integer>) finalIndex.query(point.getPoint().getEnvelopeInternal()).stream().map((Object se) -> ((AbstractMap.SimpleEntry)se).getKey()).collect(Collectors.groupingBy(x -> x, Collectors.summingInt(x -> 1)));
//                        List<Tuple2<Integer, Long>> finalPoints = points;
//                        partitions.entrySet().forEach(p -> finalPoints.add(new Tuple2<>(p.getKey(), p.getValue().longValue())));
//                        points = points.stream().collect(Collectors.groupingBy(x -> x._1, Collectors.summingLong(x -> x._2))).entrySet().stream().map(e -> new Tuple2<>(e.getKey(), e.getValue())).collect(Collectors.toList());
//                        System.out.println(points.stream().sorted(Comparator.comparingLong(p -> p._2)).collect(Collectors.toList()));
                    } else if((ProcessorType.valueOf(gridType) == (ProcessorType.SPS)) || ((ProcessorType.valueOf(gridType) == (ProcessorType.SPIS)))){
                        Iterator<Tuple2<Integer, Point>> it = null;
                        if((totalPoints == 0 || (skewPoints/totalPoints) < skewPercentage)){
                            do{
                                pointId = ThreadLocalRandom.current().nextInt(codedPoints.size());
                                point = codedPoints.get(pointId);
                            }while(!randPartition.equals(partitioner.placeObject(point.getPoint()).next()._1));//while(!worker01Partitions.contains(partitioner.placeObject(point.getPoint()).next()._1));
                            totalPoints++;
                            skewPoints++;
                        }else{
                            do {
                                pointId = ThreadLocalRandom.current().nextInt(codedPoints.size());
                                point = codedPoints.get(pointId);
                                it = partitioner.placeObject(point.getPoint());
                                while(it.hasNext()){
                                    if(it.next()._1.equals(15)){
                                        System.out.println("In 15 partition");
                                    }
                                }
                                it = partitioner.placeObject(point.getPoint());
                            }while(forbiddenPartitions.contains(it.next()._1) || (it.hasNext() && forbiddenPartitions.contains(it.next()._1)));
                            totalPoints++;
                        }
                    }

                    if(pubId.get()%1000==0) {
                        System.out.println("Current pubId: " + pubId);
                    }

//                    pointId = point.getId();

                    List<CodedGeometry> matchingAreas = areaIndex.query(point.getPoint().getEnvelopeInternal());
                    List<CodedGeometry> matchingDistricts = districtIndex.query(point.getPoint().getEnvelopeInternal());
                    List<CodedGeometry> matchingSectors = sectorIndex.query(point.getPoint().getEnvelopeInternal());

                    int conditionCounter = 0;
                    for (CodedGeometry matchingArea : matchingAreas) {
                        if (matchingArea.getGeometry().covers(point.getPoint())) {
                            conditionCounter++;
                            break;
                        }
                    }
                    for (CodedGeometry matchingDistrict : matchingDistricts) {
                        if (matchingDistrict.getGeometry().covers(point.getPoint())) {
                            conditionCounter++;
                            break;
                        }
                    }
                    for (CodedGeometry matchingSector : matchingSectors) {
                        if (matchingSector.getGeometry().covers(point.getPoint())) {
                            conditionCounter++;
                            break;
                        }
                    }
                    if (conditionCounter == 3) {
                        //point is valid
                        double type = ThreadLocalRandom.current().nextDouble();

                        if (type < areaPercentage) {
//                            System.out.println("Area created");
                            //generate area publication
                            for (CodedGeometry matchingArea : matchingAreas) {
                                if (matchingArea.getGeometry().covers(point.getPoint())) {
                                    publicationsPerTask.add(new Publication(matchingArea.getGeometry(), pubId.getAndIncrement()));
                                }
                            }

                        } else if (type < areaPercentage + districtPercentage) {
                            //generate district publication
                            for (CodedGeometry matchingDistrict : matchingDistricts) {
                                if (matchingDistrict.getGeometry().covers(point.getPoint())) {
                                    publicationsPerTask.add(new Publication(matchingDistrict.getGeometry(), pubId.getAndIncrement()));
                                }
                            }
                        } else if (type < areaPercentage + districtPercentage + sectorPercentage) {
                            //generate sector publication
                            for (CodedGeometry matchingSector : matchingSectors) {
                                if (matchingSector.getGeometry().covers(point.getPoint())) {
                                    publicationsPerTask.add(new Publication(matchingSector.getGeometry(), pubId.getAndIncrement()));
                                }
                            }
                        } else {
                            //generate point publication
                            publicationsPerTask.add(new Publication(point.getPoint(), pubId.getAndIncrement()));
                        }
                    }
                }

                return publicationsPerTask;

            };

            Collection<Callable<List<Publication>>> generateTasks = new ArrayList<>(numberOfProcessors - 1);
            for (int i = 0; i < numberOfProcessors - 1; i++) {
                generateTasks.add(generateTask);
            }

            List<List<Publication>> publicationsList = executor.invokeAll(generateTasks)
                    .stream()
                    .map(future -> {
                        try {
                            return future.get();
                        } catch (InterruptedException | ExecutionException e) {
                            throw new IllegalStateException(e);
                        }
                    })
                    .collect(Collectors.toList());

            //producing
            System.out.println("Producing...");

            Properties producerProps = new Properties();
            final String bootstrapServers = "broker01:9092,broker02:9092,broker03:9092,broker04:9092";//"localhost:9092";
            producerProps.put("bootstrap.servers", bootstrapServers);
            producerProps.put("key.serializer", IntegerSerializer.class.getName());
            producerProps.put("value.serializer", ObjectSerializer.class.getName());
            producerProps.put("acks", "all");

            //non-parallel production
            final LongAdder counter = new LongAdder();
            Collection<Callable<Void>> produceTasks = new ArrayList<>(numberOfProcessors - 1);
            for (int i = 0; i < numberOfProcessors - 1; i++) {
                //produceTasks.add(new ProduceTask(producerProps, publicationsList.get(i), topic, counter));
                try {
                    new ProduceTask(producerProps, publicationsList.get(i), topic, counter, numberOfPublications).call();
                } catch (Exception ex) {
                    //do nothing
                }
            }
            executor.invokeAll(produceTasks);
            executor.shutdown();
            System.out.println("Finished...");
        }
    }

    private static class ProduceTask implements Callable<Void> {

        private final Properties producerProps;
        private final Iterator<Publication> publicationsPerTaskIterator;
        private final String topic;
        private final LongAdder counter;
        private final int numberOfPublications;

        public ProduceTask(Properties producerProps, List<Publication> publicationsPerTask, String topic, LongAdder counter, int numberOfPublictions) {
            this.producerProps = producerProps;
            this.publicationsPerTaskIterator = publicationsPerTask.iterator();
            this.topic = topic;
            this.counter = counter;
            this.numberOfPublications = numberOfPublictions;
        }

        @Override
        public Void call() {
            Producer<Void, Publication> producer = new KafkaProducer<>(producerProps);
            while (counter.longValue() < numberOfPublications) {
                Publication publication = publicationsPerTaskIterator.next();
                ProducerRecord<Void, Publication> producerRecord = new ProducerRecord<>(topic, publication);

                try {
                    producer.send(producerRecord).get(2, TimeUnit.SECONDS);
                    counter.increment();
                    System.out.println("Publication " + counter.longValue() + " produced");
                } catch (Exception ex) {
                    //do nothing
                }
            }
            producer.close();
            return null;
        }
    }

    public static List<Integer> fillUpWorkerPartitions(ProcessorType gridType, int workers, int partitionsNum){
        Map<Integer, Integer> partitionsRatio = new HashMap<>();

        if(workers > 3) throw new IllegalArgumentException("Illegal number of workers for skewed data");
        if(gridType == ProcessorType.SPS){
            if(workers > 0){
                partitionsRatio.put(0,1);            partitionsRatio.put(17,6);            partitionsRatio.put(34,6);
                partitionsRatio.put(52,6);            partitionsRatio.put(70,6);            partitionsRatio.put(89,6);//worker01 partitions
            }
            if(workers > 1){
                partitionsRatio.put(18,6);            partitionsRatio.put(53,6);
                partitionsRatio.put(90,6);                                                                                       //worker02 partitions
            }
            if(workers > 2){
                partitionsRatio.put(19,6);            partitionsRatio.put(36,6);
                partitionsRatio.put(54,6);                                                       //worker03 partitions
            }
        }else if(gridType == ProcessorType.SPIS){
            if(workers > 0){
                partitionsRatio.put(0,1);            partitionsRatio.put(18,6);            partitionsRatio.put(36,6);
                partitionsRatio.put(54,6);            partitionsRatio.put(72,6);            partitionsRatio.put(90,6);//worker01 partitions
            }
            if(workers > 1){
                partitionsRatio.put(19,6);            partitionsRatio.put(55,6);
                partitionsRatio.put(91,6);                                                                                       //worker02 partitions
            }
            if(workers > 2){
                partitionsRatio.put(20,6);            partitionsRatio.put(38,6);
                partitionsRatio.put(56,6);                                                       //worker03 partitions
            }
        }else if(gridType == ProcessorType.RISPS && partitionsNum == 100){
            if(workers > 0){
                partitionsRatio.put(10,1);            partitionsRatio.put(28,120);            partitionsRatio.put(46,120);
                partitionsRatio.put(82,60);                                                                                      //worker10 partitions
            }
            if(workers > 1){
                partitionsRatio.put(2,120);            partitionsRatio.put(19,120);            partitionsRatio.put(72,120);
                partitionsRatio.put(90,120);                                                                                       //worker02 partitions
            }
            if(workers > 2){
                partitionsRatio.put(17,120);            partitionsRatio.put(18,120);            partitionsRatio.put(71,2);
                partitionsRatio.put(89,120);                                                                                       //worker01 partitions
            }
        }else if(gridType == ProcessorType.RISPS && partitionsNum == 200){
            if(workers > 0){
                partitionsRatio.put(0,12);            partitionsRatio.put(1,12);            //partitionsRatio.put(118,12);
                partitionsRatio.put(119,12);            //partitionsRatio.put(158,12);            partitionsRatio.put(159,12);
                partitionsRatio.put(197,12);            partitionsRatio.put(198,12);            partitionsRatio.put(39,12);
                partitionsRatio.put(40,12);            partitionsRatio.put(79,12);            partitionsRatio.put(80,12);//worker01 partitions
            }
            if(workers > 1){
                partitionsRatio.put(120,12);            partitionsRatio.put(121,12);            //partitionsRatio.put(122,12);
                partitionsRatio.put(160,12);            partitionsRatio.put(161,12);            partitionsRatio.put(199,12);
                partitionsRatio.put(2,12);            //partitionsRatio.put(200,12);            partitionsRatio.put(201,12);
                partitionsRatio.put(3,12);            partitionsRatio.put(41,12);            partitionsRatio.put(42,12);
                partitionsRatio.put(43,12);            partitionsRatio.put(81,12);            partitionsRatio.put(82,12);//worker02 partitions

            }
            if(workers > 2){
                //partitionsRatio.put(123,12);
                partitionsRatio.put(124,12);            partitionsRatio.put(162,12);
                partitionsRatio.put(163,12);            //partitionsRatio.put(164,12);
                //partitionsRatio.put(202,12);
                partitionsRatio.put(203,12);            partitionsRatio.put(44,12);            partitionsRatio.put(44,12);
                partitionsRatio.put(45,12);            partitionsRatio.put(5,12);            partitionsRatio.put(6,12);
                partitionsRatio.put(83,12);            partitionsRatio.put(84,12);            partitionsRatio.put(85,12);//worker03 partitions
            }
        }

        return partitionsRatio.entrySet().stream().flatMap(e -> {
            List<Integer> multipliedPartition = new ArrayList<>();
            for(int i = 0; i < e.getValue(); i++){multipliedPartition.add(e.getKey());}
            return multipliedPartition.stream();
        }).collect(Collectors.toList());
    }

    public static Integer findMaxPartitionFromIndex(SpatialIndex index, AIdedPoint point, List<Integer> forbiddenPartitions){
        Map<Integer, Integer> partitions = (Map<Integer, Integer>) index.query(point.getPoint().getEnvelopeInternal()).stream().map((Object se) -> ((AbstractMap.SimpleEntry)se).getKey()).collect(Collectors.groupingBy(x -> x, Collectors.summingInt(x -> 1)));
        boolean containsForbidden = partitions.entrySet().stream().filter(e -> forbiddenPartitions.contains(e.getKey())).count()!=0;
        Map.Entry e = partitions.entrySet().stream().sorted(Comparator.comparing(Map.Entry::getValue)).findFirst().orElse(null);
        return (e != null && !containsForbidden) ? (int)e.getKey() : null;
    }

    public static Map<Integer, Integer> findPartitionsFromIndex(SpatialIndex index, AIdedPoint point, List<Integer> forbiddenPartitions){
        Map<Integer, Integer> partitions = (Map<Integer, Integer>) index.query(point.getPoint().getEnvelopeInternal()).stream().map((Object se) -> ((AbstractMap.SimpleEntry)se).getKey()).collect(Collectors.groupingBy(x -> x, Collectors.summingInt(x -> 1)));
        return partitions;//.keySet().stream().collect(Collectors.toList());
    }


}

