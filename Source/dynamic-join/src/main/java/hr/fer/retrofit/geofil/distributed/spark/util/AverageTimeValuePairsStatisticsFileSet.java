package hr.fer.retrofit.geofil.distributed.spark.util;

import hr.fer.retrofit.dynamic_join.geops_util.statistics.TimeValuePairsStatisticsFile;
import hr.fer.retrofit.dynamic_join.geops_util.statistics.TimeValuePairsStatisticsFileSet;
import hr.fer.retrofit.geofil.distributed.spark.AbstractProcessor;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import scala.*;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.Double;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static hr.fer.retrofit.dynamic_join.geops_util.statistics.StatisticsComparator.*;

public class AverageTimeValuePairsStatisticsFileSet {
    public static void main(String[] args){
        if (args.length != 2 && args.length != 18 && args.length != 3 && args.length != 19) {
            System.out.println("StatisticsComparator experiment_num_1 experiment_num_2 ... experiment_num_18 [comparation_name]");
            System.exit(1);
        }

        int[] experimentNums = null;

        String comparationName = null;

        if(args.length == 2 || args.length == 3){
            experimentNums = IntStream.rangeClosed(Integer.parseInt(args[0]), Integer.parseInt(args[1])).toArray();
            comparationName = args[2];
        }else if(args.length == 18 || args.length == 19){
            String[] finalArgs = args;
            experimentNums = Stream.of(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17).mapToInt(i -> Integer.parseInt(finalArgs[i])).toArray();
            comparationName = args[18];
        }
        args = Arrays.stream(experimentNums)
                .mapToObj(String::valueOf)
                .toArray(String[]::new);

        BasicStatisticsFileSet[] basicStatistics = new BasicStatisticsFileSet[3];
        TimeValuePairsStatisticsFileSet[] throughputs = new TimeValuePairsStatisticsFileSet[3];
        TimeValuePairsStatisticsFileSet[] latencies = new TimeValuePairsStatisticsFileSet[3];

        for(int i = 0; i < 3; i++){
            basicStatistics[i] = new BasicStatisticsFileSet((new HashSet<>(
                    Arrays.asList(
                            new BasicStatisticsFile(getExperimentPath(args[i*3+0])),
                            new BasicStatisticsFile(getExperimentPath(args[i*3+1])),
                            new BasicStatisticsFile(getExperimentPath(args[i*3+2])),
                            new BasicStatisticsFile(getExperimentPath(args[i*3+3])),
                            new BasicStatisticsFile(getExperimentPath(args[i*3+4])),
                            new BasicStatisticsFile(getExperimentPath(args[i*3+5]))
                    ))));
            throughputs[i] = new TimeValuePairsStatisticsFileSet(new HashSet<>(
                    Arrays.asList(
                            new TimeValuePairsStatisticsFile(getThroughputPath(args[i*3+0])),
                            new TimeValuePairsStatisticsFile(getThroughputPath(args[i*3+1])),
                            new TimeValuePairsStatisticsFile(getThroughputPath(args[i*3+2])),
                            new TimeValuePairsStatisticsFile(getThroughputPath(args[i*3+3])),
                            new TimeValuePairsStatisticsFile(getThroughputPath(args[i*3+4])),
                            new TimeValuePairsStatisticsFile(getThroughputPath(args[i*3+5]))
                    )));
            latencies[i] = new TimeValuePairsStatisticsFileSet(new HashSet<>(
                    Arrays.asList(
                            new TimeValuePairsStatisticsFile(getLatencyPath(args[i*3+0])),
                            new TimeValuePairsStatisticsFile(getLatencyPath(args[i*3+1])),
                            new TimeValuePairsStatisticsFile(getLatencyPath(args[i*3+2])),
                            new TimeValuePairsStatisticsFile(getLatencyPath(args[i*3+3])),
                            new TimeValuePairsStatisticsFile(getLatencyPath(args[i*3+4])),
                            new TimeValuePairsStatisticsFile(getLatencyPath(args[i*3+5]))
                    )));
            if(comparationName != null){
                basicStatistics[i].setComparationName(comparationName);
                throughputs[i].setComparationName(comparationName);
                latencies[i].setComparationName(comparationName);
            }
        }

        Tuple13<Integer, Double, Double, Double, Double, Double, Double, Double, Double, Double, Double, Double, Double> tuple11 = Arrays.stream(basicStatistics)
                .map((stats) -> new Tuple13<Integer, Double, Double, Double, Double, Double, Double, Double, Double, Double, Double, Double, Double>(1,stats.getStaticSTRLThroughput(), stats.getStaticSTALThroughput(), stats.getDynamicLB_STAL_Throughput(), stats.getDynamicDB_STAL_Throughput(), stats.getDynamicDB_STRL_Throughput(), stats.getDynamicLB_STRL_Throughput(),
                                                                    stats.getStaticSTRLLatency(), stats.getStaticSTALLatency(), stats.getDynamicLB_STAL_Latency(), stats.getDynamicDB_STAL_Latency(), stats.getDynamicDB_STRL_Latency(),
                                                                    stats.getDynamicLB_STRL_Latency()))
                .reduce((t1,t2) -> new Tuple13<Integer, Double, Double, Double, Double, Double, Double, Double, Double, Double, Double, Double, Double>(t1._1()+t2._1(), t1._2()+t2._2(), t1._3()+t2._3(), t1._4()+t2._4(),
                        t1._5()+t2._5(), t1._6()+t2._6(), t1._7()+t2._7(), Double.valueOf(t1._8().toString())+Double.valueOf(t2._8().toString()), t1._9()+t2._9(), t1._10()+t2._10(), t1._11()+t2._11(), t1._12()+t2._12(), t1._13()+t2._13())).get();
        Tuple12<Double, Double, Double, Double, Double, Double, Double, Double, Double, Double, Double, Double> averageThroughputsLatencies = new Tuple12<>(
                tuple11._2()/tuple11._1(),
                tuple11._3()/tuple11._1(),
                tuple11._4()/tuple11._1(),
                tuple11._5()/tuple11._1(),
                tuple11._6()/tuple11._1(),
                tuple11._7()/tuple11._1(),
                tuple11._8()/tuple11._1(),
                tuple11._9()/tuple11._1(),
                tuple11._10()/tuple11._1(),
                tuple11._11()/tuple11._1(),
                tuple11._12()/tuple11._1(),
                tuple11._13()/tuple11._1()
        );

        List<Tuple7<Integer, Double, Double, Double, Double, Double, Double>> averageTimeThroughputs = Arrays.stream(throughputs)
                .flatMap(thr -> thr.getTimeValues().stream()).collect(Collectors.groupingBy(t -> t._1(), Collectors.toList()))
                .entrySet().stream().map(e -> new Tuple7<>(e.getKey(),
                        e.getValue().stream().mapToDouble(t4 -> t4._2()).average().getAsDouble(),
                        e.getValue().stream().mapToDouble(t4 -> t4._3()).average().getAsDouble(),
                        e.getValue().stream().mapToDouble(t4 -> t4._4()).average().getAsDouble(),
                        e.getValue().stream().mapToDouble(t4 -> t4._5()).average().getAsDouble(),
                        e.getValue().stream().mapToDouble(t4 -> t4._6()).average().getAsDouble(),
                        e.getValue().stream().mapToDouble(t4 -> t4._7()).average().getAsDouble()))
                .sorted(Comparator.comparingInt(t -> t._1()))
                .collect(Collectors.toList());

        List<Tuple7<Integer, Double, Double, Double, Double, Double, Double>> averageTimeLatencies = Arrays.stream(latencies)
                .flatMap(thr -> thr.getTimeValues().stream()).collect(Collectors.groupingBy(t -> t._1(), Collectors.toList()))
                .entrySet().stream().map(e -> new Tuple7<>(e.getKey(),
                        e.getValue().stream().mapToDouble(t4 -> t4._2()).average().getAsDouble(),
                        e.getValue().stream().mapToDouble(t4 -> t4._3()).average().getAsDouble(),
                        e.getValue().stream().mapToDouble(t4 -> t4._4()).average().getAsDouble(),
                        e.getValue().stream().mapToDouble(t4 -> t4._5()).average().getAsDouble(),
                        e.getValue().stream().mapToDouble(t4 -> t4._6()).average().getAsDouble(),
                        e.getValue().stream().mapToDouble(t4 -> t4._7()).average().getAsDouble()))
                .sorted(Comparator.comparingInt(t -> t._1()))
                .collect(Collectors.toList());

        if(averageTimeThroughputs.size() > 0) averageTimeThroughputs.remove(averageTimeThroughputs.size()-1);
        if(averageTimeLatencies.size() > 0) averageTimeLatencies.remove(averageTimeLatencies.size()-1);

        TimeValuePairsStatisticsFileSet.writeCollectiveTimeValuePairsToHdfs(null, AbstractProcessor.HDFS_AVG_THROUGHPUTS_RELATIVE_PATH,
                averageTimeThroughputs, Arrays.stream(experimentNums).boxed().collect(Collectors.toList()));

        TimeValuePairsStatisticsFileSet.writeCollectiveTimeValuePairsToHdfs(null, AbstractProcessor.HDFS_AVG_LATENCIES_RELATIVE_PATH,
                averageTimeLatencies, Arrays.stream(experimentNums).boxed().collect(Collectors.toList()));


        writeAverageComparationToHdfs(averageThroughputsLatencies, experimentNums, comparationName);
    }

    public static void writeAverageComparationToHdfs(Tuple12<Double,Double,Double,Double,Double,Double,Double,Double,Double,Double,Double,Double> tuple10, int[] experimentNums, String comparationName){
        try ( PrintWriter pw = new PrintWriter(FileSystem.get(new URI("hdfs://primary:8020"), new Configuration()).create(new Path("hdfs://"
                + "primary:8020" + AbstractProcessor.HDFS_COMPARATION_RELATIVE_PATH + (comparationName!=null ? (comparationName + "-") : "") +
                Arrays.stream(experimentNums).boxed().collect(Collectors.toList())
                        .stream().sorted(Integer::compareTo).collect(Collectors.toList()).toString().replace(" ", "").replace("[", "")
                        .replace("]", "")+ ".txt")))) {
            pw.write("Static STRL throughput: " + String.format("%.2f", tuple10._1()) + " pubs/s" + "\n");
            pw.write("Static STAL throughput: " + String.format("%.2f",tuple10._2()) + " pubs/s" + "\n");
            pw.write("Dynamic LB STAL throughput: " + String.format("%.2f", tuple10._3()) + " pubs/s" + "\n");
            pw.write("Dynamic DB STAL throughput: " + String.format("%.2f", tuple10._4()) + " pubs/s" + "\n");
            pw.write("Dynamic DB STRL throughput: " + String.format("%.2f", tuple10._5()) + " pubs/s" + "\n");
            pw.write("Dynamic LB STRL throughput: " + String.format("%.2f", tuple10._6()) + " pubs/s" + "\n");
            pw.write("Dynamic improvement in throughput: " + BasicStatisticsFileSet.valuesToImprovementsString(tuple10._3(),
                    Arrays.asList(tuple10._1(), tuple10._2(), tuple10._4(), tuple10._5(), tuple10._6()), true)
                    + "\n");
            pw.write("Static STRL latency: " + String.format("%.2f", tuple10._7()) + " ms" + "\n");
            pw.write("Static STAL latency: " + String.format("%.2f",tuple10._8()) + " ms" + "\n");
            pw.write("Dynamic LB STAL latency: " + String.format("%.2f", tuple10._9()) + " ms" + "\n");
            pw.write("Dynamic DB STAL latency: " + String.format("%.2f", tuple10._10()) + " ms" + "\n");
            pw.write("Dynamic DB STRL latency: " + String.format("%.2f", tuple10._11()) + " ms" + "\n");
            pw.write("Dynamic LB STRL latency: " + String.format("%.2f", tuple10._12()) + " ms" + "\n");
            pw.write("Dynamic improvement in latency: " +
                    BasicStatisticsFileSet.valuesToImprovementsString(tuple10._9(),
                            Arrays.asList(tuple10._7(), tuple10._8(), tuple10._10(), tuple10._11(), tuple10._12()), false)
                    + "\n");
        } catch (IOException | URISyntaxException ex) {
        }
    }
}
