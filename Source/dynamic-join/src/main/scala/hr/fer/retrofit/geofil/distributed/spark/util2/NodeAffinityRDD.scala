package hr.fer.retrofit.geofil.distributed.spark.util2

import hr.fer.retrofit.geofil.distributed.spark.util.HostsUtil
import org.apache.spark.rdd.RDD
import org.apache.spark.{Partition, TaskContext}

import java.util
import scala.collection.JavaConverters._
import scala.reflect.ClassTag

class NodeAffinityRDD[U: ClassTag](prev: RDD[U]) extends RDD[U](prev) {

  var locations : Array[String] = null

  var partitionMapping : Map[Int, String] = null;

  def this(prev: RDD[U], locations: java.util.List[String], partitionMapping: util.Map[Integer, String]) {
    this(prev)
    if(locations != null) {
      this.locations = locations.asScala.toArray
    }
    if(partitionMapping != null) {this.partitionMapping = partitionMapping.asScala.toMap.map {
      case (k, v) => (k.asInstanceOf[Int] -> v.asInstanceOf[String])
    }}
  }

  override def getPreferredLocations(split: Partition) : Seq[String] = {
    var host: Option[String] = None
    if (partitionMapping != null) host = partitionMapping.get(split.index)
    if (host != None){
      println("PrefLocByMap[" + split.index + "]: " + host.get)
      return Seq(host.get)
    }
    var preferredLocation = locations(split.index%(locations.length))
//    if (split.index > 50) preferredLocation = locations(split.index%(locations.length/2))
    println("PrefLoc[" + split.index + "]: " + preferredLocation)
    Seq(preferredLocation)
  }

  def setPartitioningMapping(partitionMapping: util.Map[Integer, String]) = {
    this.partitionMapping = partitionMapping.asScala.toMap.map {
      case (k, v) => (k.asInstanceOf[Int] -> v.asInstanceOf[String])
    };
  }

  override def compute(split: Partition, context: TaskContext): Iterator[U] = prev.compute(split, context)

  override protected def getPartitions: Array[Partition] = prev.partitions
}
