#!/bin/bash

source exp-config.sh

num_kafka_partitions=32
OLDIFS=$IFS

tex=fat
ncj=4
slw=10s
drp=10000
nrp=100000
constant="-tex $tex -ncj $ncj"

processor_d="${processor}_d"

if [ "$processor" == "sps" ]
then
	expno=0
	grtype=QUADTREE
	constant="$constant -tgd $grtype -nrp $nrp"
	mrp=27
	inp_arr=("-nsb 10k -npa 100 -npb 40k-sps-100-skew0.7"
	"-nsb 10k -npa 100 -npb 40k-sps-100-skew0.5"
	"-nsb 10k -npa 100 -npb 40k-sps-100-skew0.3")
	#"-nsb 10kUNIQUE -npa 100 -npb 40k-quadtree-100-sskew0.7 -nrp 40000"
elif [ "$processor" == "spis" ]
then
	expno=100
	grtype=QUADTREE
	intype=QUAD_TREE
	constant="$constant -tix $intype -tgd $grtype -nrp $nrp"
	mrp=31
	inp_arr=("-nsb 10k -npa 100 -npb 40k-spis-100-skew0.7"
	"-nsb 10k -npa 100 -npb 40k-spis-100-skew0.5"
	"-nsb 10k -npa 100 -npb 40k-spis-100-skew0.3")
	#"-nsb 10kUNIQUE -npa 200 -npb 20k-skew0.9 -nrp 20000")
elif [ "$processor" == "risps" ]
then
	expno=200
	grtype=KDBTREE
	intype=HPR_TREE
	constant="$constant -tix $intype -tgd $grtype -nrp $nrp"
	mrp=55
	inp_arr=("-nsb 10k -npa 200 -npb 40k-risps-200-skew0.7"
	"-nsb 10k -npa 200 -npb 40k-risps-200-skew0.5"
	"-nsb 10k -npa 200 -npb 40k-risps-200-skew0.3")
	#"-nsb 10kUNIQUE -npa 200 -npb 40k-skew0.9 -nrp 40000")
elif [ "$processor" == "rihps" ]
then
	expno=300
	intype=HPR_TREE
	constant="$constant -tix $intype -nrp $nrp"
	mrp=68
	inp_arr=("-nsb 10k -npa 100 -npb 40k-rihps-100-skew0.4"
	"-nsb 10k -npa 100 -npb 40k-rihps-100-skew0.3")
	#"-nsb 10k -npa 100 -npb 40k-rihps-100-skew0.2")
	#"-nsb 10kUNIQUE -npa 200 -npb 40k-skew0.9")
fi

for inp in "${inp_arr[@]}"
do
	max_throughputs=()
	cur_mrp=$mrp
	best_mrp=$mrp
	consistent_max_rounds=1 #this skips the search for best mrp
	starting_mrp_found=0
	while [ $consistent_max_rounds -lt 1 ]
	do
		if $(to_run_job "$expno");then spark-submit --jars $(echo lib/*.jar | tr ' ' ',') --driver-memory 32G --master yarn --class $(get_class_name "${!processor_d}") GeoPS-1.0-SNAPSHOT.jar -nex $((expno++)) $constant -drp $drp -slw $slw $inp -mrp $cur_mrp; else let "expno++"; fi

		if $(to_run_job "$expno");then spark-submit --jars $(echo lib/*.jar | tr ' ' ',') --driver-memory 32G --master yarn --class $(get_class_name "${!processor_d}") GeoPS-1.0-SNAPSHOT.jar -nex $((expno++)) $constant -drp $drp -drb false -slw $slw $inp -mrp $cur_mrp; else let "expno++"; fi
	
		if $(to_run_job "$expno");then spark-submit --jars $(echo lib/*.jar | tr ' ' ',') --driver-memory 32G --master yarn --class $(get_class_name "${!processor}") GeoPS-1.0-SNAPSHOT.jar -nex $((expno++)) $constant $inp -mrp $cur_mrp; else let "expno++"; fi
	
		sleep 1
		
		if $(to_run_job $((expno-1))) || $(to_run_job $((expno-2))) || $(to_run_job $((expno-3))); then 
			current_max=$(java -cp GeoPS-1.0-SNAPSHOT.jar:lib/* hr.fer.retrofit.geofil.distributed.spark.util.StatisticsComparator $((expno-1)) $((expno-2)) $((expno-3)) $processor)
			current_max=$(printf "%.0f" $current_max)
			
			if (( num_kafka_partitions*(cur_mrp-2) < $current_max )) && (( $current_max < (num_kafka_partitions*cur_mrp-5) )); then break; fi
			
			max_throughputs+=($max)
			IFS=$'\n'
			if (( $starting_mrp_found==0 )) && (( num_kafka_partitions*(mrp-1) > $current_max ))
			then
				cur_mrp=$(($(printf "%.0f" $current_max)/num_kafka_partitions))
				starting_mrp_found=1
				max_throughputs=()
			elif [ $(printf '%s\n' "${max_throughputs[*]}" | sort -nr | head -n1) = $current_max ]
			then
				best_mrp=$((cur_mrp++))
			else
				((consistent_max_rounds++))
			fi
			IFS=$OLDIFS
		else
			expno=$(($expno-3))
			break
		fi
		
		expno=$(($expno-3))
	done
	
	if (( $consistent_max_rounds>0 )); then
		for i in {1..3}; do
		 if $(to_run_job "$expno");then spark-submit --jars $(echo lib/*.jar | tr ' ' ',') --driver-memory 32G --master yarn --class $(get_class_name "${!processor_d}") GeoPS-1.0-SNAPSHOT.jar -nex $((expno++)) $constant -drp $drp -slw $slw $inp -mrp $best_mrp; else let "expno++"; fi

		if $(to_run_job "$expno");then spark-submit --jars $(echo lib/*.jar | tr ' ' ',') --driver-memory 32G --master yarn --class $(get_class_name "${!processor_d}") GeoPS-1.0-SNAPSHOT.jar -nex $((expno++)) $constant -drp $drp -drb false -slw $slw $inp -mrp $best_mrp; else let "expno++"; fi
		
		if $(to_run_job "$expno");then spark-submit --jars $(echo lib/*.jar | tr ' ' ',') --driver-memory 32G --master yarn --class $(get_class_name "${!processor}") GeoPS-1.0-SNAPSHOT.jar -nex $((expno++)) $constant $inp -mrp $best_mrp; else let "expno++"; fi
		
		if $(to_run_job "$expno");then spark-submit --jars $(echo lib/*.jar | tr ' ' ',') --driver-memory 32G --master yarn --class $(get_class_name "${!processor_d}") GeoPS-1.0-SNAPSHOT.jar -nex $((expno++)) $constant -drp $drp -tdb true -strl true -slw $slw $inp -mrp $best_mrp; else let "expno++"; fi
		
		if $(to_run_job "$expno");then spark-submit --jars $(echo lib/*.jar | tr ' ' ',') --driver-memory 32G --master yarn --class $(get_class_name "${!processor_d}") GeoPS-1.0-SNAPSHOT.jar -nex $((expno++)) $constant -drp $drp -tdb true -slw $slw $inp -mrp $best_mrp; else let "expno++"; fi
		
		if $(to_run_job "$expno");then spark-submit --jars $(echo lib/*.jar | tr ' ' ',') --driver-memory 32G --master yarn --class $(get_class_name "${!processor_d}") GeoPS-1.0-SNAPSHOT.jar -nex $((expno++)) $constant -drp $drp -strl true -slw $slw $inp -mrp $best_mrp; else let "expno++"; fi
		
		if $(to_run_job $((expno-1))) || $(to_run_job $((expno-2))) || $(to_run_job $((expno-3))) || $(to_run_job $((expno-4))) || $(to_run_job $((expno-5))) || $(to_run_job $((expno-6))); then 
				current_max=$(java -cp GeoPS-1.0-SNAPSHOT.jar:lib/* hr.fer.retrofit.geofil.distributed.spark.util.StatisticsComparator $((expno-1)) $((expno-2)) $((expno-3)) $((expno-4)) $((expno-5)) $((expno-6)) $processor); fi
				
		done
		#( java -cp GeoPS-1.0-SNAPSHOT.jar:lib/* hr.fer.retrofit.geofil.distributed.spark.util.AverageTimeValuePairsStatisticsFileSet $((expno-9)) $((expno-1)) $processor )
		
	fi

done

