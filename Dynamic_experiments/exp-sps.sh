#!/bin/bash

source exp-config.sh

num_kafka_partitions=32
OLDIFS=$IFS

tex=fat
ncj=4
slw=10s
drp=10000
nrp=100000
constant="-tex $tex"

processor_d="sps_d"

expno=906
grtype=QUADTREE
constant="$constant -tgd $grtype -nrp $nrp"
mrp=27
inp_arr=(
"-nsb 10k -npa 100 -npb 40k-sps-100-skew0.7 -ncj 4"
"-nsb 10k -npa 100 -npb 40k-sps-100-skew0.5 -ncj 4"
"-nsb 10k -npa 100 -npb 40k-sps-100-skew0.3 -ncj 4"
)

for inp in "${inp_arr[@]}"
do
	best_mrp=$mrp
	consistent_max_rounds=1 #this skips the search for best mrp
	
	if (( $consistent_max_rounds>0 )); then
		 if $(to_run_job "$expno");then spark-submit --jars $(echo lib/*.jar | tr ' ' ',') --driver-memory 32G --master yarn --class $(get_class_name "${!processor_d}") GeoPS-1.0-SNAPSHOT.jar -nex $((expno++)) $constant -drb false -drp $drp -slw $slw $inp -mrp $best_mrp; else let "expno++"; fi
	fi

done

