#!/bin/bash

processor=$1
shift

arguments=( "${@}" )
arguments_length="${#arguments[@]}"

# Processor names
sps=SpsProcessor
sps_d=SpsProcessorDynamic
spis=SpisProcessor
spis_d=SpisProcessorDynamic
risps=RispsProcessor
risps_d=RispsProcessorDynamic
rihps=RihpsProcessor
rihps_d=RihpsProcessorDynamic

get_class_name(){
	echo "hr.fer.retrofit.geofil.distributed.spark.$1"
}

to_run_job(){
	if [ $arguments_length != 0 ] ; then
		for i in "${arguments[@]}"
		do
		    if [ "$i" == "$1" ] ; then
			return 0
		    fi
		done
		return 1
	fi
	return 0
}

tex=fat
ncj=4
slw=10s

