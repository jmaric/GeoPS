# Dynamic Load Balancing in Stream Processing Pipelines Containing Stream-Static Joins
This repository contains the source code of Dynamic Load Balancing Solution for Stream-Static Joins. It also includes original datasets that were used to generate skewed datasets and the class to generate them, as well as Bash scripts used to run experiments.

Creating a skewed dataset can be made with the following class:

`java -cp DynamicLib-1.0-SNAPSHOT.jar hr.fer.retrofit.dynamic_join.geops_util.input_generator.SpecificSkewedPublicationProducer [kafka_topic_name] [number_of_input_events] [percentage_of_points] [skew_percentage] [processor_type] [number_of_partitions] [subscriptions_file]`

For example: `java -cp DynamicLib-1.0-SNAPSHOT.jar hr.fer.retrofit.dynamic_join.geops_util.input_generator.SpecificSkewedPublicationProducer geofil-sps-0.3 100000 1 0.3 sps 100 subscriptions.bin`

_exp-main.sh_ and _exp-sps.sh_ bash scripts are used to run experiments. By running `exp-main.sh [processor_type]`. For example: `exp-main.sh sps`
